const path = require('path');

const express = require('express');

const functionalController = require('../controllers/functional');
const monitoringController = require('../controllers/monitoring');
const statisticsController = require('../controllers/statistics');
const statusController = require('../controllers/status');
const ordersController = require('../controllers/orders');
const adminController = require('../controllers/admin');
const burninController = require('../controllers/burnin');

const router = express.Router();

router.post('/device', functionalController.postFunctionalById);

router.post('/monitoring', monitoringController.postMonitoring);

router.get('/monitoring', monitoringController.getMonitoring);

router.post('/statistics', statisticsController.postStatistics);

router.get('/statistics', statisticsController.getStatistics);

router.get('/orders', ordersController.getOrders);

router.get('/admin', adminController.getAdmin);

router.get('/burnin', burninController.getBurnin);

router.get('/burninlots', burninController.getBurninLots);

router.get('/admin/lotacceptance', adminController.getLotAcceptance);

router.get('/', statusController.getStatus);

module.exports = router;