const mysql = require('mysql2');

const pool = mysql.createPool({
    host: 'dbod-optodbplus.cern.ch',
    port: '5503',
    user: 'visitor',
    database: 'vtrxplus',
    password: 'VTRxTe5ting',
});

module.exports = pool.promise();
