const db = require("../util/database");

module.exports = class Statistics {
  static async selectParams(
    table,
    param,
    idRange,
    dateRange,
    factor,
    noOfBins,
    minBin,
    maxBin
  ) {
    let where = "";
    if (
      isNaN(idRange[0]) != 1 &&
      isNaN(idRange[1]) != 1 &&
      idRange[0] !== 0 &&
      idRange[1] !== 0
    ) {
      where = ` WHERE ${table}.idDevice >= ${idRange[0]} AND ${table}.idDevice <= ${idRange[1]}`;
    } else if (dateRange[0] !== dateRange[1]) {
      where = ` WHERE ${table}.created_at >= "${dateRange[0]}" AND ${table}.created_at <= "${dateRange[1]}"`;
    }

    let ch = "";
    let channels = 1;
    if (
      param !== "Itx1v2" &&
      param !== "Itx2v5" &&
      param !== "resp" &&
      param !== "sens"
    ) {
      ch = `${table}.ch, `;
      channels = 4;
    }

    // min and max for yield estimates
    let minmaxData = await db.execute(
      `SELECT MIN(${table}.${param} * ${factor}) AS paramMin, MAX(${table}.${param} * ${factor}) AS paramMax FROM ${table} ${where} GROUP BY ${table}.idDevice`
    );
    let maxOrdered = minmaxData[0].map(({paramMax})=> paramMax).sort().filter(Number) ;
    let minOrdered = minmaxData[0].map(({paramMin})=> paramMin).sort().filter(Number) ;
    
    if(param === 'sens'){
      maxOrdered = maxOrdered.reverse();
      minOrdered = minOrdered.reverse();
    }
    
    minmaxData = {minOrdered, maxOrdered};

    let data = await db.execute(
      `SELECT ${table}.idDevice, ${ch} ${table}.${param} * ${factor} AS param, ${table}.created_at, deviceinfos.idPanel FROM ${table} LEFT JOIN  burnin.deviceinfos ON ${table}.idDevice = deviceinfos.idDevice ${where} ORDER BY deviceinfos.idPanel`
      );
      
    if (data[0].length === 0) {
      return null;
    }

    data = data[0];

    const channelData = [];
    const channelNames = [];
    for (let i = 0; i < channels; i++) {
      if(channels === 1){
        channelNames.push('Rx');
      } else {
        channelNames.push('Ch'+parseInt(i+1));
      }
      channelData.push(new Array(noOfBins).fill(0));
    }

    // Finds unique panel names
    const panelData = [];
    const panelNames = [];
    for (let i = 0; i < data.length; i++) {
      if (channels === 1) {
        data[i].ch = 1;
      }
      if (panelNames.indexOf(data[i].idPanel) < 0) {
        panelNames.push(data[i].idPanel);
        panelData.push(new Array(noOfBins).fill(0));
      }
    }

    // Bins data into data bins and sorts with panel and channel names
    const binWidth = (maxBin-minBin)/noOfBins;

    for (let i = 0; i < data.length; i++) {
      let binNo = Math.floor((data[i].param-minBin)/binWidth);
      binNo = Math.min(binNo, noOfBins-1);
      binNo = Math.max(binNo, 0);
      channelData[data[i].ch-1][binNo]++
      panelData[panelNames.indexOf(data[i].idPanel)][binNo]++;
    }

    // Massage data to right format for plotting, i.e. array of objects {x, y}
    for(let dataPoint of channelData){
      for(let i = 0; i < dataPoint.length; i++){
        dataPoint[i] = {x: minBin + binWidth * i, y: dataPoint[i]}
      }
    }
    for(let dataPoint of panelData){
      for(let i = 0; i < dataPoint.length; i++){
        dataPoint[i] = {x: minBin + binWidth * i, y: dataPoint[i]}
      }
    }

    panelNames[panelNames.indexOf(null)] = "Pre-series";

    return { channelData, panelData, channelNames, panelNames, minmaxData };
  }
};
