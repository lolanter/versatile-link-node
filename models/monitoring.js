const db = require("../util/database");

module.exports = class Monitoring {
  static async selectParams(table, param, min, max, idRange, dateRange, factor) {
    let where = "";
    if (
      isNaN(idRange[0]) != 1 &&
      isNaN(idRange[1]) != 1 &&
      idRange[0] !== 0 &&
      idRange[1] !== 0
    ) {
      where = ` WHERE idDevice >= ${idRange[0]} AND idDevice <= ${idRange[1]}`;
    } else if (dateRange[0] !== dateRange[1]) {
      where = ` WHERE created_at >= "${dateRange[0]}" AND created_at <= "${dateRange[1]}"`;
    }

    let ch = "";
    let channels = 1;
    if (
      param !== "Itx1v2" &&
      param !== "Itx2v5" &&
      param !== "resp" &&
      param !== "sens"
    ) {
      ch = `${table}.ch, `;
      channels = 4;
    }
    
    let dataDate = await db.execute(
      `SELECT ${table}.idDevice, ${ch} ${table}.${param} * ${factor} AS ${param}, ${table}.created_at FROM ${table} ${where} ORDER BY ${table}.created_at`
    );

    let dataId = await db.execute(
      `SELECT ${table}.idDevice, ${ch} ${table}.${param} * ${factor} AS ${param}, ${table}.created_at FROM ${table} ${where} ORDER BY ${table}.idDevice`
    );

    if (dataDate[0].length === 0) {
      return null;
    }

    dataDate = dataDate[0];
    dataId = dataId[0];

    let datasetId = new Array(channels);
    let datasetDate = new Array(channels);
    for(let ch = 0; ch < datasetId.length; ch++){
      datasetId[ch] = [];
      datasetDate[ch] = [];
    }

    for (let i = 0; i < dataDate.length; i++) {
      let channel = typeof dataDate[i].ch === "undefined" ? 0 : dataDate[i].ch - 1;
    
      if (dataDate[i][param] > min && dataDate[i][param] < max) {
        datasetDate[channel].push({
          x: dataDate[i].created_at,
          y: dataDate[i][param],
        });
      }
      if (dataId[i][param] > min && dataId[i][param] < max) {
        datasetId[channel].push({
          x: dataId[i].idDevice,
          y: dataId[i][param],
        });
      }
    }

    return { datasetId, datasetDate };
  }
};
