const db = require("../util/database");
const dbqa = require("../util/databaseQA");

module.exports = class Functional {
  static async selectLIbyId(id, qa) {
    let data;
    if (qa) {
      data = await dbqa.execute(
        `SELECT * FROM latestLIdata WHERE idDevice = ${id} ORDER BY ch ASC`
      );
    } else {
      data = await db.execute(
        `SELECT * FROM latestLIdata WHERE idDevice = ${id} ORDER BY ch ASC`
      );
    }
    if (data[0].length === 0) {
      return null;
    }

    let lidata = data[0];

    for (const channel of lidata) {
      let dataBias = [];
      let dataSet = [];

      const PoptData = channel.PoptData;
      const IbiasData = channel.IbiasData;

      let IbiasArray = IbiasData.split(";").map(Number);
      let PoptArray = PoptData.split(";").map(Number);
      let biasSetArray = [1, 2, 3, 4, 5, 16, 32, 48, 88, 127];

      for (let i = 0; i < IbiasArray.length; i++) {
        let dataElem = {};
        let setElem = {};
        dataElem.x = 1000 * IbiasArray[i];
        dataElem.y = 1000 * PoptArray[i];
        setElem.x = biasSetArray[i];
        setElem.y = 1000 * PoptArray[i];
        dataBias.push(dataElem);
        dataSet.push(setElem);
      }

      channel.dataBias = dataBias;
      channel.dataSet = dataSet;
    }

    return lidata;
  }

  static async selectBERbyId(id, qa) {
    let data;
    if (qa) {
      data = await dbqa.execute(
        `SELECT * FROM latestBERdata WHERE idDevice = ${id}`
      );
    } else {
      data = await db.execute(
        `SELECT * FROM latestBERdata WHERE idDevice = ${id}`
      );
    }

    if (data[0].length === 0) {
      return null;
    }

    let berData = data[0][0];

    let berArray = berData.berdata.split(";").map(Number);
    data[0][0];

    let berCurve = [];
    let berCurveSens = [];

    let berCurveWatts = [];
    let berCurveSensWatts = [];

    if (berData.sensqa !== null) {
      berCurveSens.push({
        x: berData.sensqa,
        y: 1e-12,
      });
      berCurveSens.push({
        x: berData.sens,
        y: 1e-10,
      });
      berCurveSensWatts.push({
        x: dBm2uWatts(berData.sensqa),
        y: 1e-12,
      });
      berCurveSensWatts.push({
        x: dBm2uWatts(berData.sens),
        y: 1e-10,
      });
    } else {
      berCurveSens.push({
        x: berData.sens,
        y: 1e-12,
      });
      berCurveSensWatts.push({
        x: dBm2uWatts(berData.sens),
        y: 1e-12,
      });
    }

    berCurveSens.push({
      x: berArray[2 + 5],
      y: berArray[2],
    });
    berCurveSensWatts.push({
      x: dBm2uWatts(berArray[2 + 5]),
      y: berArray[2],
    });

    for (let i = 0; i < berArray[0]; i++) {
      berCurve.push({
        x: berArray[2 + 5 + i * berArray[1]],
        y: berArray[2 + i * berArray[1]],
      });
      berCurveWatts.push({
        x: dBm2uWatts(berArray[2 + 5 + i * berArray[1]]),
        y: berArray[2 + i * berArray[1]],
      });
    }

    berData.berCurve = berCurve;
    berData.berCurveSens = berCurveSens;
    berData.berCurveWatts = berCurveWatts;
    berData.berCurveSensWatts = berCurveSensWatts;

    berData.sensWatts = dBm2uWatts(berData.sens);
    berData.sensqaWatts = dBm2uWatts(berData.sensqa);

    return berData;
  }

  static async selectRSSIbyId(id, qa) {
    let data;
    if (qa) {
      data = await dbqa.execute(
        `SELECT * FROM latestRSSIdata WHERE idDevice = ${id}`
      );
    } else {
      data = await db.execute(
        `SELECT * FROM latestRSSIdata WHERE idDevice = ${id}`
      );
    }

    if (data[0].length === 0) {
      return null;
    }

    let rssidata = data[0][0];

    let IrssiArray = rssidata.Irssidata.split(";").map(Number);
    let PrssiArray = rssidata.Prssidata.split(";").map(Number);

    let rssi = [];

    for (let i = 0; i < IrssiArray.length; i++) {
      let dataElem = {};
      dataElem.x = 1000 * PrssiArray[i];
      dataElem.y = 1000 * IrssiArray[i];
      rssi.push(dataElem);
    }

    rssidata.data = rssi;

    return rssidata;
  }

  static async selectEYEbyId(id, qa) {
    let data;
    if (qa) {
      data = await dbqa.execute(
        `SELECT * FROM latestEYEdata WHERE idDevice = ${id} ORDER BY ch ASC`
      );
    } else {
      data = await db.execute(
        `SELECT * FROM latestEYEdata WHERE idDevice = ${id} ORDER BY ch ASC`
      );
    }
    if (data[0].length === 0) {
      return null;
    }

    let UI = (1 / 10.24e9) * 1e12;
    let graphHW = UI; // graph half width

    let eyedata = data[0];

    for (const channel of eyedata) {
 
      channel.omaxp === null ? channel.omaxp = 0 : channel.omaxp = channel.omaxp * 1000;
      channel.averagepower === null ? channel.averagepower = 0 : channel.averagepower = channel.averagepower * 1000;
      channel.eyeheight === null ? channel.eyeheight = 0 : channel.eyeheight = channel.eyeheight * 1000;
      channel.risetime === null ? channel.risetime = 0 : channel.risetime = channel.risetime * 1e12;
      channel.falltime === null ? channel.falltime = 0 : channel.falltime = channel.falltime * 1e12;
      channel.eyewidth === null ? channel.eyewidth = 0 : channel.eyewidth = channel.eyewidth * 1e12;
      channel.jitterpp === null ? channel.jitterpp = 0 : channel.jitterpp = channel.jitterpp * 1e12;
      channel.jitterrms === null ? channel.jitterrms = 0 : channel.jitterrms =channel.jitterrms * 1e12;
      channel.maskmargin === null ? channel.maskmargin = 0 : channel.maskmargin =channel.maskmargin;
      
      let topTrace = [];
      let bottomTrace = [];
      let oneTrace = [];
      let zeroTrace = [];
      let eyeOpeningTrace = [];
      let eyeOpeningLeftTrace = [];
      let eyeOpeningRightTrace = [];

      let totalRiseTime = (channel.risetime / 0.8) ;
      let totalFallTime = (channel.falltime / 0.8);

      let riseSlope = totalRiseTime / channel.omaxp;
      let fallSlope = totalFallTime / channel.omaxp;

      topTrace.push({
        x: -graphHW,
        y: channel.averagepower - 0.5 * channel.omaxp,
      });
      topTrace.push({
        x: -0.5 * UI - 0.5 * totalRiseTime,
        y: channel.averagepower - 0.5 * channel.omaxp,
      });
      topTrace.push({
        x: -0.5 * UI + 0.5 * totalRiseTime,
        y: channel.averagepower + 0.5 * channel.omaxp,
      });
      topTrace.push({
        x: 0.5 * UI - 0.5 * totalFallTime,
        y: channel.averagepower + 0.5 * channel.omaxp,
      });
      topTrace.push({
        x: 0.5 * UI + 0.5 * totalFallTime,
        y: channel.averagepower - 0.5 * channel.omaxp,
      });
      topTrace.push({
        x: graphHW,
        y: channel.averagepower - 0.5 * channel.omaxp,
      });

      bottomTrace.push({
        x: -graphHW,
        y: channel.averagepower + 0.5 * channel.omaxp,
      });
      bottomTrace.push({
        x: -0.5 * UI - 0.5 * totalFallTime,
        y: channel.averagepower + 0.5 * channel.omaxp,
      });
      bottomTrace.push({
        x: -0.5 * UI + 0.5 * totalFallTime,
        y: channel.averagepower - 0.5 * channel.omaxp,
      });
      bottomTrace.push({
        x: 0.5 * UI - 0.5 * totalRiseTime,
        y: channel.averagepower - 0.5 * channel.omaxp,
      });
      bottomTrace.push({
        x: 0.5 * UI + 0.5 * totalRiseTime,
        y: channel.averagepower + 0.5 * channel.omaxp,
      });
      bottomTrace.push({
        x: graphHW,
        y: channel.averagepower + 0.5 * channel.omaxp,
      });

      oneTrace.push({
        x: -graphHW,
        y: channel.averagepower - 0.5 * channel.omaxp,
      });
      oneTrace.push({
        x: graphHW,
        y: channel.averagepower - 0.5 * channel.omaxp,
      });

      zeroTrace.push({
        x: -graphHW,
        y: channel.averagepower + 0.5 * channel.omaxp,
      });
      zeroTrace.push({
        x: graphHW,
        y: channel.averagepower + 0.5 * channel.omaxp,
      });

      eyeOpeningTrace.push({
        x: -0.5 * channel.eyewidth,
        y: channel.averagepower,
      });
      eyeOpeningTrace.push({
        x: -0.5 * channel.eyewidth + riseSlope * 0.5 * channel.eyeheight,
        y: channel.averagepower + 0.5 * channel.eyeheight,
      });
      eyeOpeningTrace.push({
        x: 0.5 * channel.eyewidth - fallSlope * 0.5 * channel.eyeheight,
        y: channel.averagepower + 0.5 * channel.eyeheight,
      });
      eyeOpeningTrace.push({
        x: 0.5 * channel.eyewidth,
        y: channel.averagepower,
      });
      eyeOpeningTrace.push({
        x: 0.5 * channel.eyewidth - riseSlope * 0.5 * channel.eyeheight,
        y: channel.averagepower - 0.5 * channel.eyeheight,
      });
      eyeOpeningTrace.push({
        x: -0.5 * channel.eyewidth + fallSlope * 0.5 * channel.eyeheight,
        y: channel.averagepower - 0.5 * channel.eyeheight,
      });
      eyeOpeningTrace.push({
        x: -0.5 * channel.eyewidth,
        y: channel.averagepower,
      });

      eyeOpeningLeftTrace.push({
        x: -graphHW,
        y: channel.averagepower + 0.5 * channel.eyeheight,
      });
      eyeOpeningLeftTrace.push({
        x:
          -0.5 * channel.eyewidth -
          (UI - channel.eyewidth) -
          fallSlope * 0.5 * channel.eyeheight,
        y: channel.averagepower + 0.5 * channel.eyeheight,
      });
      eyeOpeningLeftTrace.push({
        x: -0.5 * channel.eyewidth - (UI - channel.eyewidth),
        y: channel.averagepower,
      });
      eyeOpeningLeftTrace.push({
        x:
          -0.5 * channel.eyewidth -
          (UI - channel.eyewidth) -
          riseSlope * 0.5 * channel.eyeheight,
        y: channel.averagepower - 0.5 * channel.eyeheight,
      });
      eyeOpeningLeftTrace.push({
        x: -graphHW,
        y: channel.averagepower - 0.5 * channel.eyeheight,
      });

      eyeOpeningRightTrace.push({
        x: graphHW,
        y: channel.averagepower + 0.5 * channel.eyeheight,
      });
      eyeOpeningRightTrace.push({
        x:
          0.5 * channel.eyewidth +
          (UI - channel.eyewidth) +
          riseSlope * 0.5 * channel.eyeheight,
        y: channel.averagepower + 0.5 * channel.eyeheight,
      });
      eyeOpeningRightTrace.push({
        x: 0.5 * channel.eyewidth + (UI - channel.eyewidth),
        y: channel.averagepower,
      });
      eyeOpeningRightTrace.push({
        x:
          0.5 * channel.eyewidth +
          (UI - channel.eyewidth) +
          fallSlope * 0.5 * channel.eyeheight,
        y: channel.averagepower - 0.5 * channel.eyeheight,
      });
      eyeOpeningRightTrace.push({
        x: graphHW,
        y: channel.averagepower - 0.5 * channel.eyeheight,
      });

      channel.topTrace = topTrace;
      channel.bottomTrace = bottomTrace;
      channel.zeroTrace = zeroTrace;
      channel.oneTrace = oneTrace;
      channel.eyeOpeningTrace = eyeOpeningTrace;
      channel.eyeOpeningLeftTrace = eyeOpeningLeftTrace;
      channel.eyeOpeningRightTrace = eyeOpeningRightTrace;
    }

    return eyedata;
  }
};

function dBm2uWatts(dbm) {
  return (10 ** (dbm / 10) / 1000) * 1e6;
}
