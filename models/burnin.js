const db = require("../util/database");
const dbqa = require("../util/databaseQA");

module.exports = class Burnin {
  static async selectPtyp(idLot, type) {

    const panelLocOffset = 16

    let database = 'latestPOSTdata'
    if (type == 'PRE') {
      database = 'latestPREdata'
    }

    let data;
    if (type == 'DELTA') {
      data = await db.execute(
        `SELECT MOD(latestPOSTdata.idDevice-${panelLocOffset},40) as panelLocation, latestPOSTdata.ch, latestPOSTdata.Ptyp/latestPREdata.Ptyp AS Ptyp FROM burnin.latestPOSTdata JOIN burnin.latestPREdata ON latestPOSTdata.idDevice=latestPREdata.idDevice AND latestPOSTdata.ch=latestPREdata.ch WHERE latestPOSTdata.idLot = ${idLot} AND latestPOSTdata.Ptyp > 0.1 AND latestPREdata.Ptyp > 0.1  ORDER BY panelLocation, ch`
      );
    } else {
      data = await db.execute(
        `SELECT MOD(idDevice-${panelLocOffset},40) as panelLocation, ch, Ptyp FROM burnin.${database} WHERE idLot = ${idLot} AND Ptyp > 0.1 ORDER BY panelLocation, ch`
      );
    }

    if (data[0].length === 0) {
      return null;
    }
    let PtypData = data[0];

    let dataAvg;
    dataAvg = await db.execute(
      `SELECT MOD(idDevice-${panelLocOffset},40) as panelLocation, AVG(Ptyp) AS Pavg FROM burnin.${database} WHERE idLot = ${idLot} AND Ptyp > 0.1 GROUP BY panelLocation ORDER BY panelLocation `
    );
    if (dataAvg[0].length === 0) {
      return null;
    }
    let PtypAvg = dataAvg[0];

    return { PtypData, PtypAvg };
  }

  static async selectFails(idLot) {

    const panelLocOffset = 16

    let data = await db.execute(
      `SELECT MIN(deviceinfos.idPanel) AS idPanel, MOD(MIN(latestPREdata.idDevice)-${panelLocOffset},40) as panelLocation, MIN(latestPOSTdata.Ptyp) AS PpostMin, MIN(latestPREdata.Ptyp) AS PpreMin, MIN(latestPOSTdata.Ptyp/latestPREdata.Ptyp) AS PtypMin, MAX(latestPOSTdata.Ptyp/latestPREdata.Ptyp) AS PtypMax, MAX(latestPOSTdata.failStatus) AS postFailStatus, MAX(latestPREdata.failStatus) AS preFailStatus FROM burnin.latestPOSTdata JOIN burnin.latestPREdata ON latestPOSTdata.idDevice=latestPREdata.idDevice AND latestPOSTdata.ch=latestPREdata.ch JOIN burnin.deviceinfos ON deviceinfos.idDevice=latestPREdata.idDevice WHERE latestPREdata.idLot = ${idLot} GROUP BY latestPREdata.idDevice ORDER BY latestPREdata.idDevice`
    );

    let panelIds = []
    let panelIndex = 0

    let panelPreFailStatus = new Array(40).fill(0);
    let panelPostFailStatus = new Array(40).fill(0);
    let panelMinFails = new Array(40).fill(0);
    let panelMaxFails = new Array(40).fill(0);
    let panelPreNulls = new Array(40).fill(0);
    let panelPostNulls = new Array(40).fill(0);
    let sumMinFails = new Array(40).fill(0);
    let sumMaxFails = new Array(40).fill(0);
    let allPanelsMinFails = []
    let allPanelsMaxFails = []
    let allPanelsPreFailStatus = []
    let allPanelsPostFailStatus = []

    for (let dataPoint of data[0]) {
      if (panelIds[panelIndex - 1] != dataPoint.idPanel) {
        if (panelIndex != 0) {
          allPanelsMinFails.push(panelMinFails)
          allPanelsMaxFails.push(panelMaxFails)
          allPanelsPreFailStatus.push(panelPreFailStatus)
          allPanelsPostFailStatus.push(panelPostFailStatus)
        }

        panelMinFails = new Array(40).fill(0);
        panelMaxFails = new Array(40).fill(0);
        panelPreFailStatus = new Array(40).fill(0);
        panelPostFailStatus = new Array(40).fill(0);
        panelIds.push(dataPoint.idPanel)
        panelIndex++
      }

      panelPreFailStatus[dataPoint.panelLocation] = dataPoint.preFailStatus
      panelPostFailStatus[dataPoint.panelLocation] = dataPoint.postFailStatus

      if (dataPoint.PpreMin < 0.1) {
        panelPreNulls[dataPoint.panelLocation]++
      } else if (dataPoint.PpostMin < 0.1) {
        panelPostNulls[dataPoint.panelLocation]++
      } else if (dataPoint.PtypMin < 0.85 && dataPoint.PtypMin != null) {
        panelMinFails[dataPoint.panelLocation]++
        sumMinFails[dataPoint.panelLocation]++
      } else if (dataPoint.PtypMax > 1.05 && dataPoint.PtypMax != null) {
        panelMaxFails[dataPoint.panelLocation]++
        sumMaxFails[dataPoint.panelLocation]++
      }

    }

    allPanelsMinFails.push(panelMinFails)
    allPanelsMaxFails.push(panelMaxFails)
    allPanelsPreFailStatus.push(panelPreFailStatus)
    allPanelsPostFailStatus.push(panelPostFailStatus)

    let sumPostFailStatus = new Array(40).fill(0);
    for (let panelFailStatus of allPanelsPostFailStatus) {
      for (let i = 0; i < panelFailStatus.length; i++) {
        sumPostFailStatus[i] += panelFailStatus[i]
      }
    }

    return { panelIds, sumMaxFails, sumMinFails, panelPreNulls, panelPostNulls, allPanelsMaxFails, allPanelsMinFails, allPanelsPreFailStatus, allPanelsPostFailStatus, sumPostFailStatus };
  }

  static async selectLots() {
    let data;
    data = await db.execute(
      `SELECT SUBSTRING(CONVERT(idPanel, CHAR),8,7) as "Lot", count(*), min(created_at) AS created FROM burnin.deviceinfos WHERE dutStatus = 0 AND idDevice > 2015 GROUP BY Lot ORDER BY created DESC`
    );

    if (data[0].length === 0) {
      return null;
    }

    let Lots = data[0];

    return Lots;
  }

  static async selectBurninStatus() {

    let sapData = await db.execute(
      `SELECT SUBSTRING(CONVERT(idPanel, CHAR),8,7) as lot , count(failStatus) fails FROM burnin.sapdata WHERE failStatus = 1 GROUP BY SUBSTRING(CONVERT(idPanel, CHAR),8,7) ORDER BY lot`
    );
    sapData = sapData[0]
    let preData = await db.execute(
      `SELECT lot, count(moduleFailStatus) AS fails FROM (SELECT latestPREdata.idDevice, max(latestPREdata.failStatus) AS moduleFailStatus, SUBSTRING(CONVERT(idPanel, CHAR),8,7) as "Lot" FROM burnin.latestPREdata JOIN  burnin.deviceinfos ON deviceinfos.idDevice = latestPREdata.idDevice GROUP BY idDevice) AS t WHERE t.moduleFailStatus = 1 GROUP BY lot ORDER BY lot;`
    );
    preData = preData[0]
    let postData = await db.execute(
      `SELECT lot, count(moduleFailStatus) AS fails FROM (SELECT latestPOSTdata.idDevice, max(latestPOSTdata.failStatus) AS moduleFailStatus, SUBSTRING(CONVERT(idPanel, CHAR),8,7) as "Lot" FROM burnin.latestPOSTdata JOIN  burnin.deviceinfos ON deviceinfos.idDevice = latestPOSTdata.idDevice GROUP BY idDevice) AS t WHERE t.moduleFailStatus = 1 GROUP BY lot ORDER BY lot;`
    );
    postData = postData[0]

    let allData = await db.execute(
      `SELECT lot, count(moduleFailStatus) AS fails FROM (SELECT latestPREdata.idDevice, max(latestPREdata.failStatus) AS moduleFailStatus, SUBSTRING(CONVERT(idPanel, CHAR),8,7) as "Lot" FROM burnin.latestPREdata JOIN  burnin.deviceinfos ON deviceinfos.idDevice = latestPREdata.idDevice GROUP BY idDevice) AS t GROUP BY lot ORDER BY lot;`
    );
    allData = allData[0]

    let lots = []
    let sap = []
    let pre = []
    let post = []
    let all = []

    let index = 0
    for (let preLot of preData) {
      if(parseInt(preLot.lot) < 1113268){
        continue
      }
      lots.push(preLot.lot)
      
      all.push(allData[index].fails)
      
      pre.push({x: preLot.lot, y: preLot.fails/allData[index].fails*100})
      
      let sapFails = null
      for (let sapLot of sapData) {
        if(sapLot.lot == preLot.lot){
          sapFails = sapLot.fails
        }
      }
      sap.push({x: preLot.lot, y: sapFails/allData[index].fails*100})

      let postFails = null
      for (let postLot of postData) {
        if(postLot.lot == preLot.lot){
          postFails = postLot.fails
        }
      }
      post.push({x: preLot.lot, y: postFails/allData[index].fails*100})

      index++
    }

    return {lots, sap, pre, post, all};
  }

}



