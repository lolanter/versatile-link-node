const db = require("../util/database");

module.exports = class Orders {
    static async selectUsers(roles) {

        // Gets all experiment names in the databese
        let userList = await db.execute(
            `SELECT min(experiment) as experiment FROM vtrxplusplanning.user GROUP BY experiment;`
        );
        let experimentArray = []
        for (let user of userList[0]) {
            experimentArray.push(user.experiment)
        }

        let where = createWhere(roles, experimentArray)

        let userArray = []

        if (where != '()') {
            let users = await db.execute(`SELECT experiment, detector FROM vtrxplusplanning.user WHERE ${where};`);
            userArray = users[0]
        }

        return { userArray };
    }

    static async selectMissingReception() {
        let batch_id = await db.execute(`SELECT batch.batch_id FROM vtrxplusplanning.batch JOIN vtrxplusplanning.inventory ON inventory.batch_id = batch.batch_id WHERE batch.reception_date IS NULL AND inventory.delivery_id > 0 GROUP BY batch.batch_id;`);
        batch_id = batch_id[0]

        return batch_id;
    }

    static async selectOrderedETAs() {
        let batch_id = await db.execute(`SELECT batch.batch_id, batch.eta_date, order.order_date FROM vtrxplusplanning.batch JOIN vtrxplusplanning.order ON order.batch_id = batch.batch_id WHERE batch.reception_date IS NULL;`);
        batch_id = batch_id[0]

        return batch_id;
    }

    static async selectRequests(roles, detector) {

        // Gets all experiment names in the databese
        let userList = await db.execute(
            `SELECT min(experiment) as experiment FROM vtrxplusplanning.user GROUP BY experiment;`
        );
        let experimentArray = []
        for (let user of userList[0]) {
            experimentArray.push(user.experiment)
        }

        let where = createWhere(roles, experimentArray)

        let requests = await db.execute(`SELECT * FROM vtrxplusplanning.request JOIN vtrxplusplanning.user ON user.user_id = request.user_id JOIN vtrxplusplanning.product ON product.product_id = request.product_id WHERE ${where} AND user.detector = '${detector}' ORDER BY request.product_id, request.request_date;`);
        let requestData = requests[0]

        let user_id = 0
        if (requestData.length !== 0) {
            user_id = requestData[0].user_id
        }

        let total_requests = await db.execute(
            `SELECT request.product_id, SUM(request.quantity) AS request_quantity FROM vtrxplusplanning.request  WHERE request.user_id = ${user_id} GROUP BY request.product_id;`
        );
        let total_deliveries = await db.execute(
            `SELECT delivery.product_id, total_length, SUM(delivery.quantity) AS delivery_quantity FROM vtrxplusplanning.delivery JOIN vtrxplusplanning.product ON delivery.product_id = product.product_id WHERE delivery.user_id = ${user_id} GROUP BY delivery.product_id;`
        );

        for (let total_request of total_requests[0]) {
            for (let total_delivery of total_deliveries[0]) {
                if (total_delivery.product_id === total_request.product_id) {
                    total_request.delivery_quantity = total_delivery.delivery_quantity
                    total_request.total_length = total_delivery.total_length
                }
            }
        }

        let deliveries = await db.execute(`SELECT inventory.delivery_id, MIN(inventory.batch_id) AS batch_id, MIN(inventory.product_id) AS product_id, COUNT(inventory.delivery_id) AS quantity, COUNT(inventory.delivery_id) AS remaining, MAX(delivery.delivery_date) AS delivery_date, GROUP_CONCAT(delivery.comment) AS delivery_comment, GROUP_CONCAT(inventory.comment) AS inventory_comment, MAX(batch.eta_date) AS eta_date, MAX(batch.reception_date) AS reception_date FROM vtrxplusplanning.inventory JOIN vtrxplusplanning.delivery ON delivery.delivery_id = inventory.delivery_id JOIN vtrxplusplanning.batch ON inventory.batch_id = batch.batch_id WHERE delivery.user_id = ${user_id} GROUP BY inventory.delivery_id, inventory.product_id`);
        deliveries = deliveries[0]

        for (let request of requestData) {

            let orders = await db.execute(`SELECT * FROM vtrxplusplanning.order JOIN vtrxplusplanning.batch ON order.batch_id = batch.batch_id WHERE order.request_id = '${request.request_id}';`);
            orders = orders[0]

            request.orders = orders

            request.delivered = 0
            request.remaining = request.quantity
            request.deliveries = []
            request.qtyToDeliver = []
            request.qtyPerDelivery = []

            let orderBatchIDs = []

            for (let i = 0; i < deliveries.length; i++) {

                let order = await db.execute(`SELECT * FROM vtrxplusplanning.order JOIN vtrxplusplanning.batch ON batch.batch_id = order.batch_id WHERE order.batch_id = ${deliveries[i].batch_id} AND order.request_id = ${request.request_id};`);
                if (order[0][0] != undefined) {
                    orderBatchIDs.push(order[0][0].batch_id)
                }

                if (deliveries[i].product_id === request.product_id && deliveries[i].remaining > 0 && request.quantity != request.delivered) {

                    if (deliveries[i].remaining > (request.quantity - request.delivered)) {

                        deliveries[i].remaining = deliveries[i].remaining - (request.quantity - request.delivered)
                        request.qtyPerDelivery.push((request.quantity - request.delivered))
                        request.qtyToDeliver.push(0)
                        request.delivered = request.quantity
                        request.deliveries.push(deliveries[i])
                    } else {
                        request.delivered += deliveries[i].remaining
                        request.qtyPerDelivery.push(deliveries[i].remaining)
                        request.qtyToDeliver.push(request.quantity - request.delivered)
                        deliveries[i].remaining = 0
                        request.deliveries.push(deliveries[i])
                    }
                }
            }
        }

        // get plot data
        let productIds = await db.execute(`SELECT product_id FROM vtrxplusplanning.request WHERE user_id = ${user_id} GROUP BY product_id;`);

        let numOfProducts = productIds[0].length

        let plotRequest = []
        for (let product_id of productIds[0]) {
            let data = await db.execute(`SELECT quantity AS y, request_date AS x FROM vtrxplusplanning.request WHERE user_id = ${user_id} AND product_id = ${product_id.product_id} ORDER BY request_date;`);
            let qty = 0
            for (let dataPoint of data[0]) {
                qty += dataPoint.y
                dataPoint.y = qty
            }
            plotRequest.push(data[0])
        }

        let plotOrder = []
        let lastOrderQty = []
        let i = 0
        for (let product_id of productIds[0]) {
            let data = await db.execute(`SELECT quantity AS y, order_date AS x FROM vtrxplusplanning.order WHERE user_id = ${user_id} AND product_id = ${product_id.product_id} AND order_state = 2 ORDER BY order_date;`);
            let qty = 0
            for (let dataPoint of data[0]) {
                qty += dataPoint.y
                dataPoint.y = qty
                lastOrderQty[i] = dataPoint.y
            }
            data[0].splice(0, 0, { y: 0, x: plotRequest[i][0].x })
            plotOrder.push(data[0])
            i++
        }

        let plotPendingOrder = []
        i = 0
        for (let product_id of productIds[0]) {
            let data = await db.execute(`SELECT quantity AS y, order_date AS x FROM vtrxplusplanning.order WHERE user_id = ${user_id} AND product_id = ${product_id.product_id} AND order_state = 1 ORDER BY order_date;`);
            let qty = 0
            for (let dataPoint of data[0]) {
                qty += dataPoint.y
                dataPoint.y = qty + lastOrderQty[i]
            }
            data[0].splice(0, 0, { y: plotOrder[i][plotOrder[i].length - 1].y, x: plotOrder[i][plotOrder[i].length - 1].x })
            plotPendingOrder.push(data[0])
            i++
        }

        let plotReception = []
        let lastReceptionDate = []
        let lastReceptionQty = []
        i = 0

        for (let product_id of productIds[0]) {
            let data = await db.execute(`SELECT order.quantity AS y, batch.reception_date AS x FROM vtrxplusplanning.order JOIN vtrxplusplanning.batch ON order.batch_id = batch.batch_id WHERE order.user_id = ${user_id} AND order.product_id = ${product_id.product_id} ORDER BY batch.reception_date;`);
            let qty = 0
            for (let dataPoint of data[0]) {
                if (dataPoint.x != null) {
                    qty += dataPoint.y
                } else {
                    dataPoint.x = plotRequest[i][0].x
                }
                dataPoint.y = qty
                lastReceptionDate[i] = dataPoint.x
                lastReceptionQty[i] = dataPoint.y
            }
            data[0].splice(0, 0, { y: 0, x: plotRequest[i][0].x })
            plotReception.push(data[0])
            i++
        }

        let plotEta = []
        i = 0
        for (let product_id of productIds[0]) {
            if (lastReceptionDate[i] == undefined) {
                lastReceptionDate[i] = new Date()
            }
            let data = await db.execute(`SELECT order.quantity AS y, IFNULL(batch.reception_date, batch.eta_date) AS x FROM vtrxplusplanning.order JOIN vtrxplusplanning.batch ON order.batch_id = batch.batch_id WHERE order.user_id = ${user_id} AND order.product_id = ${product_id.product_id} AND batch.eta_date >= '${lastReceptionDate[i].toISOString()}' ORDER BY batch.eta_date;`);
            let qty = 0
            for (let dataPoint of data[0]) {
                if (dataPoint.x != null) {
                    qty += dataPoint.y
                } else {
                    dataPoint.x = plotRequest[i][0].x
                }
                dataPoint.y = qty + lastReceptionQty[i]
            }
            data[0].splice(0, 0, { y: lastReceptionQty[i], x: lastReceptionDate[i] })
            plotEta.push(data[0])
            i++
        }

        let plotDelivery = []
        i = 0
        for (let product_id of productIds[0]) {
            let data = await db.execute(`SELECT quantity AS y, delivery_date AS x FROM vtrxplusplanning.delivery WHERE user_id = ${user_id} AND product_id = ${product_id.product_id} ORDER BY delivery_date;`);
            let qty = 0
            for (let dataPoint of data[0]) {
                if (dataPoint.x != null) {
                    qty += dataPoint.y
                } else {
                    dataPoint.x = plotRequest[i][0].x
                }
                dataPoint.y = qty
            }
            data[0].splice(0, 0, { y: 0, x: plotRequest[i][0].x })
            plotDelivery.push(data[0])
            i++
        }

        let plotData = { plotRequest, plotOrder, plotPendingOrder, plotEta, plotReception, plotDelivery }

        return { requestData, total_requests, plotData, numOfProducts };
    }

    static async selectRequestsAdmin() {

        let requestData = []
        let total_requests = []

        // get plot data
        let numOfProducts = 1

        let plotRequest = []
        
        let data = await db.execute(`SELECT quantity AS y, request_date AS x FROM vtrxplusplanning.request ORDER BY request_date;`);
        let qty = 0
        for (let dataPoint of data[0]) {
            qty += dataPoint.y
            dataPoint.y = qty
        }
        plotRequest.push(data[0])
        
        let plotOrder = []
        let lastOrderQty = []
        data = await db.execute(`SELECT quantity AS y, order_date AS x FROM vtrxplusplanning.order WHERE order_state = 2 ORDER BY order_date;`);
        qty = 0
        for (let dataPoint of data[0]) {
            qty += dataPoint.y
            dataPoint.y = qty
            lastOrderQty[0] = dataPoint.y
        }
        data[0].splice(0, 0, { y: 0, x: plotRequest[0][0].x })
        plotOrder.push(data[0])
        
        let plotPendingOrder = []
        data = await db.execute(`SELECT quantity AS y, order_date AS x FROM vtrxplusplanning.order WHERE order_state = 1 ORDER BY order_date;`);
        qty = 0
        for (let dataPoint of data[0]) {
            qty += dataPoint.y
            dataPoint.y = qty + lastOrderQty[0]
        }
        data[0].splice(0, 0, { y: plotOrder[0][plotOrder[0].length - 1].y, x: plotOrder[0][plotOrder[0].length - 1].x })
        plotPendingOrder.push(data[0])
        
        let plotReception = []
        let lastReceptionDate = []
        let lastReceptionQty = []
        data = await db.execute(`SELECT order.quantity AS y, batch.reception_date AS x FROM vtrxplusplanning.order JOIN vtrxplusplanning.batch ON order.batch_id = batch.batch_id ORDER BY batch.reception_date;`);
        qty = 0
        for (let dataPoint of data[0]) {
            if (dataPoint.x != null) {
                qty += dataPoint.y
            } else {
                dataPoint.x = plotRequest[0][0].x
            }
            dataPoint.y = qty
            lastReceptionDate[0] = dataPoint.x
            lastReceptionQty[0] = dataPoint.y
        }
        data[0].splice(0, 0, { y: 0, x: plotRequest[0][0].x })
        plotReception.push(data[0])
        
        let plotEta = []
        if (lastReceptionDate[0] == undefined) {
            lastReceptionDate[0] = new Date()
        }
        data = await db.execute(`SELECT order.quantity AS y, IFNULL(batch.reception_date, batch.eta_date) AS x FROM vtrxplusplanning.order JOIN vtrxplusplanning.batch ON order.batch_id = batch.batch_id WHERE  batch.eta_date >= '${lastReceptionDate[0].toISOString()}' ORDER BY batch.eta_date;`);
        qty = 0
        for (let dataPoint of data[0]) {
            if (dataPoint.x != null) {
                qty += dataPoint.y
            } else {
                dataPoint.x = plotRequest[0][0].x
            }
            dataPoint.y = qty + lastReceptionQty[0]
        }
        data[0].splice(0, 0, { y: lastReceptionQty[0], x: lastReceptionDate[0] })
        plotEta.push(data[0])
        
        let plotDelivery = []
        data = await db.execute(`SELECT quantity AS y, delivery_date AS x FROM vtrxplusplanning.delivery ORDER BY delivery_date;`);
        qty = 0
        for (let dataPoint of data[0]) {
            if (dataPoint.x != null) {
                qty += dataPoint.y
            } else {
                dataPoint.x = plotRequest[0][0].x
            }
            dataPoint.y = qty
        }
        data[0].splice(0, 0, { y: 0, x: plotRequest[0][0].x })
        plotDelivery.push(data[0])
        
        let plotData = { plotRequest, plotOrder, plotPendingOrder, plotEta, plotReception, plotDelivery }

        return { requestData, total_requests, plotData, numOfProducts };
    }

};

function createWhere(roles, experimentArray) {

    let where = '('
    // Create query limit
    for (let role of roles) {
        let roleExperiment = role.split('-')[0].toUpperCase()

        for (let experiment of experimentArray) {
            if (experiment == roleExperiment || roles.includes('admin-role')) {
                if (where != '(') {
                    where += ' OR '
                }
                where += "experiment = '" + experiment + "'"
            }
        }
    }
    where += ')'
    return where;
}


