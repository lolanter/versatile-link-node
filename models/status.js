const db = require("../util/database");

module.exports = class Statistics {
  static async selectPasses() {
    let passDataMonth = await db.execute(
      `SELECT COUNT(idDevice) AS pass, YEAR(created_at) as y, MONTH(created_at) AS m FROM latestdevicestatuses WHERE pass = 1 GROUP BY YEAR(created_at), MONTH(created_at);`
    );

    let failDataMonth = await db.execute(
      `SELECT COUNT(idDevice) AS fail, YEAR(created_at) as y, MONTH(created_at) AS m FROM latestdevicestatuses WHERE pass = 0 GROUP BY YEAR(created_at), MONTH(created_at);`
    );

    passDataMonth = passDataMonth[0];
    failDataMonth = failDataMonth[0];

    // Fill empty months
    // for (let i = 0; i < passDataMonth.length - 1; i++) {
    //   if (
    //     passDataMonth[i].m < 12 &&
    //     passDataMonth[i + 1].m !== passDataMonth[i].m + 1
    //   ) {
    //     passDataMonth.splice(i + 1, 0, {
    //       pass: 0,
    //       y: passDataMonth[i].y,
    //       m: passDataMonth[i].m + 1,
    //     });
    //   } else if (passDataMonth[i].m === 12 && passDataMonth[i + 1].m !== 1) {
    //     passDataMonth.splice(i + 1, 0, {
    //       pass: 0,
    //       y: passDataMonth[i].y + 1,
    //       m: 1,
    //     });
    //   }
    // }
    // for (let i = 0; i < failDataMonth.length - 1; i++) {
    //   if (
    //     failDataMonth[i].m < 12 &&
    //     failDataMonth[i + 1].m !== failDataMonth[i].m + 1
    //   ) {
    //     failDataMonth.splice(i + 1, 0, {
    //       fail: 0,
    //       y: failDataMonth[i].y,
    //       m: failDataMonth[i].m + 1,
    //     });
    //   } else if (failDataMonth[i].m === 12 && failDataMonth[i + 1].m !== 1) {
    //     failDataMonth.splice(i + 1, 0, {
    //       fail: 0,
    //       y: failDataMonth[i].y + 1,
    //       m: 1,
    //     });
    //   }
    // }

    passDataMonth = passDataMonth.map((x) => ({
      x: new Date().setFullYear(x.y, x.m, 1),
      y: x.pass,
    }));
    failDataMonth = failDataMonth.map((x) => ({
      x: new Date().setFullYear(x.y, x.m, 1),
      y: x.fail,
    }));

    let passDataDay = await db.execute(
      `SELECT COUNT(idDevice) AS pass, DATE(created_at) as d FROM latestdevicestatuses WHERE pass = 1 AND created_at > DATE_ADD(CURDATE(), INTERVAL -4 WEEK) GROUP BY DATE(created_at);`
    );

    let failDataDay = await db.execute(
      `SELECT COUNT(idDevice) AS fail, DATE(created_at) as d FROM latestdevicestatuses WHERE pass = 0 AND created_at > DATE_ADD(CURDATE(), INTERVAL -4 WEEK) GROUP BY DATE(created_at);`
    );

    passDataDay = passDataDay[0].map((x) => ({ x: x.d, y: x.pass }));
    failDataDay = failDataDay[0].map((x) => ({ x: x.d, y: x.fail }));

    return { passDataMonth, failDataMonth, passDataDay, failDataDay };
  }


  static async selectRunningTotal() {
    let data = await db.execute(
      `SELECT SUM(pass) as pass, (COUNT(pass)-SUM(pass)) as fail, DATE(created_at) as x FROM latestdevicestatuses WHERE created_at > '2023-04-01' GROUP BY DATE(created_at) ORDER BY DATE(created_at);`
    );
    
    let yieldData = await db.execute(
      `SELECT SUM(fail) OVER (ORDER BY created_at ASC RANGE INTERVAL 30 DAY PRECEDING) AS fail, SUM(pass) OVER (ORDER BY created_at ASC RANGE INTERVAL 30 DAY PRECEDING) AS pass, DATE(created_at) as x FROM (SELECT SUM(pass) as pass, (COUNT(pass)-SUM(pass)) as fail, DATE(created_at) as created_at FROM latestdevicestatuses WHERE created_at > '2023-04-01' GROUP BY DATE(created_at) ORDER BY DATE(created_at)) AS table1;`
    ); 

    let passRunning = [];
    let failRunning = [];
    let yieldRunning = [];

    let pass = [];
    let fail = [];

    let sumPass = 0;
    let sumFail = 0;
    for(let datapoint of data[0]){
      sumPass += parseInt(datapoint.pass);
      passRunning.push({x: datapoint.x, y: sumPass});
      sumFail += parseInt(datapoint.fail);
      failRunning.push({x: datapoint.x, y: sumFail});
      //yieldRunning.push({x: datapoint.x, y: sumPass/(sumFail+sumPass)*100});

      pass.push({x: datapoint.x, y: parseInt(datapoint.pass)})
      fail.push({x: datapoint.x, y: parseInt(datapoint.fail)})
    }

    for(let datapoint of yieldData[0]){
      yieldRunning.push({x: datapoint.x, y: parseInt(datapoint.pass)/(parseInt(datapoint.pass)+parseInt(datapoint.fail))*100});
    }

    return { passfail: {pass, fail}, running: {passRunning, failRunning, yieldRunning} };
  }

};
