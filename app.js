const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const cron = require('node-cron');

const isAdmin = require('./middleware/isAdmin');
const errorController = require('./controllers/error');
const adminController = require('./controllers/admin');
const db = require('./util/database');

const app = express();

const routes = require('./routes/routes');

// EJS view engine
app.set('view engine', 'ejs');
app.set('views', 'views');

// Parse incoming request bodies in a middleware before your handlers
// urlencoded: returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use(isAdmin);

app.use(routes);

app.use('/500', errorController.get500);

// Catches all page not found issues
app.use(errorController.get404);

app.use((error, req, res, next) => {
    console.log(error);
    res.redirect('/500');
});

app.listen(3000);

// Schedule the cron job to run every minute
    cron.schedule('0 5 * * *', () => {
    adminController.getCheckNewLots();
});


