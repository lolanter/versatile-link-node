const Statistics = require("../models/statistics");

exports.getStatistics = async (req, res, next) => {

  try {

    const paramNames = [];

    const idRange = [];
    const dateRange = [];
    const category = "";

    res.render("statistics", {
      pageTitle: "Statistics",
      path: "/statistics",
      roles: req.roles,
      paramNames,
      ranges: JSON.stringify(0),
      dataParam1: JSON.stringify(0),
      dataParam2: JSON.stringify(0),
      category, 
      idRange, 
      dateRange
    });
  } catch (err) {
    const error = new Error(err);
    error.httpStatusCode = 500;
    return next(error);
  }
};


exports.postStatistics = async (req, res, next) => {
  const category = req.body.category;
  const idMin = parseInt(req.body.idMin);
  const idMax = parseInt(req.body.idMax);
  const dateMin = req.body.dateMin;
  const dateMax = req.body.dateMax;

  const idRange = [idMin, idMax];
  const dateRange = [dateMin, dateMax];
  
  let tables = [];
  let params = [];
  let paramNames = [];
  let factor;
  let noOfBins = [];
  let minBin = [];
  let maxBin = [];
  let rangeMin = [];
  let rangeMax = [];
  if(category === 'Rx'){
    tables = ['latestRSSIdata', 'latestBERdata'];
    params = ['resp', 'sens'];
    paramNames = ['Responsivity','Sensitivity'];
    factor = 1;
    noOfBins = [40, 80];
    minBin = [0, -20];
    maxBin = [1, 0];
    rangeMin = [0.3, -18];
    rangeMax = [0.8, -13];
  } else if (category === 'Icc'){
    tables = ['latestpowerdata', 'latestpowerdata'];
    params = ['Itx1v2', 'Itx2v5'];
    paramNames = ['Supply 1.2 V','Supply 2.5 V'];
    factor = 1000;
    noOfBins = [40, 40];
    minBin = [10, 10];
    maxBin = [20, 20];
    rangeMin = [11,  11];
    rangeMax = [17, 17];
  } else if (category === 'TxLI'){
    tables = ['latestLIdata', 'latestLIdata'];
    params = ['Ith', 'PoptTyp'];
    paramNames = ['Threshold Current','Typical Power'];
    factor = 1000;
    noOfBins = [40, 40];
    minBin = [0, 0];
    maxBin = [1, 2];
    rangeMin = [0.4, 0];
    rangeMax = [1, 1.8];
  } else if (category === 'amplitude'){
    tables = ['latestEYEdata', 'latestEYEdata'];
    params = ['omaxp', 'averagepower'];
    paramNames = ['OMA','Average Power'];
    factor = 1000;
    noOfBins = [40, 40];
    minBin = [0, 0];
    maxBin = [2, 2];
    rangeMin = [0.2, 0.2];
    rangeMax = [1.8, 1.8];
  } else if (category === 'time'){
    tables = ['latestEYEdata', 'latestEYEdata'];
    params = ['risetime', 'falltime'];
    paramNames = ['Rise Time','Fall Time'];
    factor = 1e12;
    noOfBins = [40, 40];
    minBin = [10, 10];
    maxBin = [50, 50];
    rangeMin = [20, 20];
    rangeMax = [45, 45];
  } else if (category === 'jitter'){
    tables = ['latestEYEdata', 'latestEYEdata'];
    params = ['jitterpp', 'jitterrms'];
    paramNames = ['Jitter PkPk','Jitter RMS'];
    factor = 1e12;
    noOfBins = [40, 40];
    minBin = [0, 0];
    maxBin = [30, 10];
    rangeMin = [5, 0.5];
    rangeMax = [30, 5];
  } 
    
  const ranges = JSON.stringify({rangeMin, rangeMax});

  try {

    let Param1data = await Statistics.selectParams(tables[0], params[0], idRange, dateRange, factor, noOfBins[0], minBin[0], maxBin[0]);
    let dataJSONParam1 = JSON.stringify(Param1data);

    let Param2data = await Statistics.selectParams(tables[1], params[1], idRange, dateRange, factor, noOfBins[1], minBin[1], maxBin[1]);
    let dataJSONParam2 = JSON.stringify(Param2data);

    res.render("statistics", {
      pageTitle: "Statistics",
      path: "/statistics",
      roles: req.roles,
      dataParam1: dataJSONParam1,
      dataParam2: dataJSONParam2,
      paramNames,
      ranges,
      category, 
      idRange, 
      dateRange
    });
  } catch (err) {
    const error = new Error(err);
    error.httpStatusCode = 500;
    return next(error);
  }
};
