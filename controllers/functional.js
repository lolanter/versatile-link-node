const Functional = require("../models/functional");

exports.postFunctionalById = async (req, res, next) => {
  const idDevice = parseInt(req.body.idDevice);
  const qa = parseInt(req.body.qa);

  try {
    let functionalData = [];
       
    let lidata = await Functional.selectLIbyId(idDevice, qa);
    let berdata = await Functional.selectBERbyId(idDevice, qa);
    let rssidata = await Functional.selectRSSIbyId(idDevice, qa);
    let eyedata = await Functional.selectEYEbyId(idDevice, qa);

    functionalData.push({ lidata, berdata, rssidata, eyedata });
    
    let dataJSON = JSON.stringify(functionalData);

    let noData = 1;
    let acceptanceData = 0;
    if (
      lidata !== null ||
      berdata !== null ||
      rssidata !== null ||
      eyedata !== null
    ) {
      let acceptanceLIdata = await Functional.selectLIbyId(idDevice, 1);
      acceptanceLIdata !== null ?  acceptanceData = 1 : acceptanceData = 0;
      noData = 0;
    }

    res.render("functional", {
      pageTitle: "Functional",
      path: "/functional",
      roles: req.roles,
      idDevice: idDevice,
      noData: noData,
      data: dataJSON,
      functionalData: functionalData,
      qa: qa,
      acceptanceData: acceptanceData,
    });
  } catch (err) {
    const error = new Error(err);
    error.httpStatusCode = 500;
    return next(error);
  }
};
