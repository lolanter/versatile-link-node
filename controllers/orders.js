const Orders = require("../models/orders");

exports.getOrders = async (req, res, next) => {
  const detector = req.query.detector;
  const idOrder = req.query.idOrder;
  const roles = req.roles

  try {
 
    let users = await Orders.selectUsers(roles);

    let experiment = ''
    for(let user of users.userArray){
      if(user.detector === detector){
        experiment = user.experiment
      }
    }

    let requestData = []
    let totalRequests = []
    let plotData
    let numOfProducts = 0
    if(detector != undefined){
      let data = await Orders.selectRequests(roles, detector)      
      requestData = data.requestData
      totalRequests = data.total_requests[0]
      plotData = JSON.stringify(data.plotData)
      numOfProducts = data.numOfProducts
    } else if(roles.includes('admin-role')){
      let data = await Orders.selectRequestsAdmin()      
      plotData = JSON.stringify(data.plotData)
      numOfProducts = data.numOfProducts
      totalRequests.push({total_length: 'All'})
    }

    res.render("orders", {
      pageTitle: "Orders",
      path: "/orders",
      roles: req.roles,
      requestData: requestData,
      users: users.userArray,
      totalRequests: totalRequests,
      detector: detector,
      experiment: experiment,
      numOfProducts,
      plotData,
      idOrder
    });
    
  } catch (err) {
    const error = new Error(err);
    error.httpStatusCode = 500;
    return next(error);
  }

};
