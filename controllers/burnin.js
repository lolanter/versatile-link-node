const Burnin = require("../models/burnin");
const db = require("../util/database");

exports.getBurnin = async (req, res, next) => {
  let idLot = parseInt(req.query.idLot);
  try {

    let lots = await Burnin.selectLots();

    let statusData = await Burnin.selectBurninStatus()
    let statusJSON = JSON.stringify(statusData)

    res.render("burnin", {
      pageTitle: "Burn-in",
      path: "/burnin",
      roles: req.roles,
      lots: lots,
      idLot,
      statusData: statusJSON
    });
  } catch (err) {
    const error = new Error(err);
    error.httpStatusCode = 500;
    return next(error);
  }
};

exports.getBurninLots = async (req, res, next) => {
  let idLot = parseInt(req.query.idLot);
  try {

    let lots = await Burnin.selectLots();

    if (isNaN(idLot)) {
      idLot = parseInt(lots[0].Lot)
    }

    let createdArray = []
    for (let lot of lots) {
      if (parseInt(lot.Lot) == idLot) {
        createdArray = lot.created.toString().split(" ")
      }
    }
    let created = createdArray[0] + " " + createdArray[1] + " " + createdArray[2] + " " + createdArray[3]

    const types = ['DELTA', 'PRE', 'POST']

    let failsData = await Burnin.selectFails(idLot)

    let allData = []

    for (const type of types) {

      let data = await Burnin.selectPtyp(idLot, type);

      let PtypData = []
      let PavgData = []

      if (data !== null) { // This if statement is blind fix
        PtypData = data.PtypData
        PavgData = data.PtypAvg
      }

      let labels = new Array()
      let dataArrayCh1 = new Array()
      let dataArrayCh2 = new Array()
      let dataArrayCh3 = new Array()
      let dataArrayCh4 = new Array()
      let dataLocMedian = new Array()

      for (let i = 0; i < 40; i++) {
        labels.push(i + 1);
        dataArrayCh1[i] = [];
        dataArrayCh2[i] = [];
        dataArrayCh3[i] = [];
        dataArrayCh4[i] = [];
        dataLocMedian[i] = [];
      }

      for (const dataPoint of PtypData) {
        dataLocMedian[dataPoint.panelLocation].push(dataPoint.Ptyp)
      }

      for (const dataPoint of PtypData) {

        let normalize = 1
        if (type != 'DELTA') {
          let locationPtypsSorted = dataLocMedian[dataPoint.panelLocation].sort()
          normalize = locationPtypsSorted[Math.round(dataLocMedian[dataPoint.panelLocation].length / 2)]
        }

        if (dataPoint.ch == 1) {
          dataArrayCh1[dataPoint.panelLocation].push(dataPoint.Ptyp / normalize)
        } else if (dataPoint.ch == 2) {
          dataArrayCh2[dataPoint.panelLocation].push(dataPoint.Ptyp / normalize)
        } else if (dataPoint.ch == 3) {
          dataArrayCh3[dataPoint.panelLocation].push(dataPoint.Ptyp / normalize)
        } else if (dataPoint.ch == 4) {
          dataArrayCh4[dataPoint.panelLocation].push(dataPoint.Ptyp / normalize)
        }

      }

      data = { labels, dataArrayCh1, dataArrayCh2, dataArrayCh3, dataArrayCh4 }
      allData.push(data)
    }

    allData.push(failsData)

    let dataJSON = JSON.stringify(allData)

    res.render("burninlots", {
      pageTitle: "Burn-in",
      path: "/burnin",
      roles: req.roles,
      lots: lots,
      idLot,
      failsData,
      created,
      burninData: dataJSON,

    });
  } catch (err) {
    const error = new Error(err);
    error.httpStatusCode = 500;
    return next(error);
  }
};
