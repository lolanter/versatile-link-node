const Status = require("../models/status");

exports.getStatus = async (req, res, next) => {

  try {

    let data = await Status.selectRunningTotal();
    let dataJSON = JSON.stringify(data);
  
    res.render("status", {
      pageTitle: "Status",
      path: "/status",
      roles: req.roles,
      dataJSON: dataJSON
    });
  } catch (err) {
    const error = new Error(err);
    error.httpStatusCode = 500;
    return next(error);
  }
};

