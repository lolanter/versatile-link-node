const Monitoring = require("../models/monitoring");

exports.getMonitoring = async (req, res, next) => {

  try {

    const paramNames = [];

    const idRange = [];
    const dateRange = [];
    const category = "";
    const timeSpan ="";

    res.render("monitoring", {
      pageTitle: "Monitoring",
      path: "/monitoring",
      roles: req.roles,
      paramNames,
      ranges: JSON.stringify(0),
      dataParam1: JSON.stringify(0),
      dataParam2: JSON.stringify(0),
      idRange,
      dateRange,
      timeSpan,
      category
    });
  } catch (err) {
    const error = new Error(err);
    error.httpStatusCode = 500;
    return next(error);
  }
};


exports.postMonitoring = async (req, res, next) => {
  const category = req.body.category;
  const idMin = parseInt(req.body.idMin);
  const idMax = parseInt(req.body.idMax);
  let dateMin = req.body.dateMin;
  let dateMax = req.body.dateMax;

  const idRange = [idMin, idMax];
  const dateRange = [dateMin, dateMax];
  
  let timeSpan = 100;
  if(dateMin !== "" && dateMax !== ""){
    dateMin = new Date(dateMin);
    dateMax = new Date(dateMax);

    timeSpan = (dateMax.getTime() - dateMin.getTime())/(1000*3600*24);
  }

  let tables = [];
  let params = [];
  let paramNames = [];
  let mins = [];
  let maxs = [];
  let factor;
  if(category === 'Rx'){
    tables = ['latestRSSIdata', 'latestBERdata'];
    params = ['resp', 'sens'];
    paramNames = ['Responsivity','Sensitivity'];
    mins = [0.2, -18];
    maxs = [0.7, -12];
    factor = 1;
  } else if (category === 'Icc'){
    tables = ['latestpowerdata', 'latestpowerdata'];
    params = ['Itx1v2', 'Itx2v5'];
    paramNames = ['Supply 1.2 V','Supply 2.5 V'];
    mins = [12, 12];
    maxs = [18, 18];
    factor = 1000;
  } else if (category === 'TxLI'){
    tables = ['latestLIdata', 'latestLIdata'];
    params = ['Ith', 'PoptTyp'];
    paramNames = ['Threshold Current','Typical Power'];
    mins = [0, 0];
    maxs = [1.5, 2];
    factor = 1000;
  } else if (category === 'amplitude'){
    tables = ['latestEYEdata', 'latestEYEdata'];
    params = ['omaxp', 'averagepower'];
    paramNames = ['OMA','Average Power'];
    mins = [0, 0];
    maxs = [2, 2];
    factor = 1000;
  } else if (category === 'time'){
    tables = ['latestEYEdata', 'latestEYEdata'];
    params = ['risetime', 'falltime'];
    paramNames = ['Rise Time','Fall Time'];
    mins = [0, 0];
    maxs = [50, 50];
    factor = 1e12;
  } else if (category === 'jitter'){
    tables = ['latestEYEdata', 'latestEYEdata'];
    params = ['jitterpp', 'jitterrms'];
    paramNames = ['Jitter PkPk','Jitter RMS'];
    mins = [0, 0];
    maxs = [50, 30];
    factor = 1e12;
  } 
    
  const ranges = JSON.stringify({mins, maxs});

  try {

    let Param1data = await Monitoring.selectParams(tables[0], params[0], mins[0], maxs[0], idRange, dateRange, factor);
    let dataJSONParam1 = JSON.stringify(Param1data);

    let Param2data = await Monitoring.selectParams(tables[1], params[1], mins[1], maxs[1], idRange, dateRange, factor);
    let dataJSONParam2 = JSON.stringify(Param2data);

    res.render("monitoring", {
      pageTitle: "Monitoring",
      path: "/monitoring",
      roles: req.roles,
      dataParam1: dataJSONParam1,
      dataParam2: dataJSONParam2,
      paramNames,
      ranges,
      idRange,
      dateRange,
      timeSpan,
      category
    });
  } catch (err) {
    const error = new Error(err);
    error.httpStatusCode = 500;
    return next(error);
  }
};
