const db = require("../util/database");
const Orders = require("../models/orders");

const nodemailer = require("nodemailer");

exports.getAdmin = async (req, res, next) => {
  try {

    let batch_ids = await Orders.selectMissingReception();

    let ete_batch_ids = await Orders.selectOrderedETAs();

    for(let batch_id of batch_ids){
      for(let i = 0; i < ete_batch_ids.length; i++){
        if(ete_batch_ids[i].batch_id == batch_id.batch_id){
          ete_batch_ids.splice(i,1)
          i--
        }
      }
    }

    res.render("admin", {
      pageTitle: "Admin",
      path: "/admin",
      roles: req.roles,
      batch_ids,
      ete_batch_ids
    });
  } catch (err) {
    const error = new Error(err);
    error.httpStatusCode = 500;
    return next(error);
  }
};

exports.getLotAcceptance = async (req, res, next) => {
  try {
    res.render("lotacceptance", {
      pageTitle: "Lot Acceptance",
      path: "/admin/lotacceptance",
      roles: req.roles,
    });
  } catch (err) {
    const error = new Error(err);
    error.httpStatusCode = 500;
    return next(error);
  }
};

exports.getCheckNewLots = async (req, res, next) => {

  let data = await db.execute(
    `SELECT idLot FROM burnin.latestPREdata ORDER BY created_at DESC LIMIT 1;`
  );

  let latestLot = data[0][0].idLot

  let dataPost;
  dataPost = await db.execute(
    `SELECT MIN(idPanel), MAX(latestPOSTdata.created_at) AS created_at FROM burnin.latestPOSTdata JOIN burnin.deviceinfos ON latestPOSTdata.idDevice = deviceinfos.idDevice WHERE latestPOSTdata.idLot = ${latestLot} GROUP BY idPanel ORDER BY created_at DESC;`
  );

  dataPre = await db.execute(
    `SELECT MIN(idPanel), MAX(latestPREdata.created_at) AS created_at FROM burnin.latestPREdata JOIN burnin.deviceinfos ON latestPREdata.idDevice = deviceinfos.idDevice WHERE latestPREdata.idLot = ${latestLot} GROUP BY idPanel ORDER BY created_at DESC;`
  );


  if (dataPost[0].length === dataPre[0].length) {

    let hoursSinceLastTest = (Date.now() - dataPost[0][0].created_at.getTime()) / (1000 * 60 * 60)

    if (hoursSinceLastTest < 24) {
      sendEmail(hoursSinceLastTest, latestLot)
      return 1;
    }
  }

  return 0;

};

function sendEmail(hoursSinceLastTest, latestLot) {
  // Create a transporter object using your email provider’s SMTP settings
  
  //const transporter = nodemailer.createTransport({
  //  service: "gmail",
  //  auth: {
  //    user: "vlplus.database@gmail.com", // Your email address
  //    pass: "bhjd cizq yqgd jyqp" // "njfd749njds&(hR4*", // Your email password
  //  },
  //});

  const transporter = nodemailer.createTransport({
    host: 'cernmx.cern.ch',
    port: 25,
    auth: {
      user: "vtrx.test@cern.ch", 
      pass: "OptoLink21" 
    },
  });

  // Define the email content
  const mailOptions = {
    from: "vlplus-noreply@cern.ch",
    to: ["lauri.olantera@cern.ch","csaba.soos@cern.ch","jan.troska@cern.ch"],
    subject: "VTRx+ production notice",
    text: `A new lot has gone through the burn-in. Post-testing of lot ${latestLot} was completed ${Math.round(hoursSinceLastTest*10)/10} hours ago. Data available at https://vlplus.app.cern.ch/admin/burnin`,
  };

  // Send the email
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.error("Error sending email: " + error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
}