module.exports = (req, res, next) => {
  let rolesList = req.headers["x-forwarded-groups"];

  let roles = [''];
  if (rolesList !== undefined) {
    roles = rolesList.split(",");
  }

  req.roles = roles;
  return next();
};
