//import {
//  scaleLinear,
//  extent,
//  axisLeft,
//  axisBottom,
//} from "d3";

//import d3 from "https://unpkg.com/d3?module";
import "https://unpkg.com/d3@7.8.2";

export const scatterPlot = () => {
  let aspect;
  let data;
  let xValue;
  let yValue;
  let height;
  let width;
  let groupBy;
  let margin;
  let radius;
  let ranges;
  let ticks;

  let my = (svg) => {
    /*     svg.node().parentNode;

    // get container + svg aspect ratio
     var container = d3.select(svg.node().parentNode),
      width = parseInt(svg.style("width")),
      height = width * aspect;

    // add viewBox and preserveAspectRatio properties,
    // and call resize so that svg resizes on inital page load
    svg
      .attr("viewBox", "0 0 " + width + " " + height)
      .attr("perserveAspectRatio", "xMinYMid")
      .call(resize);

    // to register multiple listeners for same event type,
    // you need to add namespace, i.e., 'click.foo'
    // necessary if you call invoke this function for multiple svgs
    // api docs: https://github.com/mbostock/d3/wiki/svgs#on
    d3.select(window).on("resize." + container.attr("id"), resize);

    // get width of container and resize svg to fit it
    function resize() {
      var targetWidth = parseInt(container.style("width"));
      svg.attr("width", targetWidth);
      svg.attr("height", Math.round(targetWidth * aspect));
    }  */

    const xPositionScale = d3
      .scaleLinear()
      .domain([ranges.xMin, ranges.xMax])
      .range([margin.left, width - margin.right]);

    const yPositionScale = d3
      .scaleLinear()
      .domain([ranges.yMin, ranges.yMax])
      .range([height - margin.bottom, 0 + margin.top]);

    // TRANSITIONS
    const t = d3.transition().duration(500).ease(d3.easeLinear);

    // Draw the lines
    let line = d3
      .line()
      .x((d) => xPositionScale(xValue(d)))
      .y((d) => yPositionScale(yValue(d))); // this will just create one continuous line through all points. Let's group!
    let grouped = Array.from(
      d3.group(data, (d) => groupBy(d)),
      ([key, values]) => ({
        key,
        values,
      })
    );

    const colorScale = d3
      .scaleOrdinal(d3.schemeCategory10)
      .domain(grouped.map((d) => d.key));

    svg
      .selectAll("path")
      .data(grouped)
      .join(
        (enter) =>
          enter
            .append("path")
            .attr("d", (d) => line(d.values))
            .attr("fill", "none")
            .attr("stroke", (d) => colorScale(d.key))
            .attr("stroke-width", 0)
            .call((enter) => enter.transition(t).attr("stroke-width", 1)), // enter selection needs to return enter selection, so calling it again with transition inside gives us that
        (update) =>
          update.call((update) =>
            update
              .transition(t).attr("d", (d) => line(d.values))
          ),
        (exit) => exit.remove()
      );

    // Draw the circles

    // Generates data mark pairs with pixel coordinates for plotting
    const datapoints = data.map((d) => ({
      x: xPositionScale(xValue(d)),
      y: yPositionScale(yValue(d)),
    }));
    //

    const positionCircles = (circles) => {
      circles.attr("cx", (d) => d.x).attr("cy", (d) => d.y);
    };

    svg
      .selectAll("circle")
      .data(datapoints)
      .join(
        (enter) =>
          enter
            .append("circle")
            .attr("cx", (d) => d.x) // These two could be called with one function .call(positionCircles) see function above and execution below in update
            .attr("cy", (d) => d.y) //
            .attr("r", 0)
            .call((enter) => enter.transition(t).attr("r", radius)), // enter selection needs to return enter selection, so calling it again with transition inside gives us that
        (update) =>
          update.call((update) =>
            update
              .transition(t)
              .call(positionCircles)
          ),
        (exit) => exit.remove()
      );

    const xTicks = d3.range(
      ticks.xStart,
      ranges.xMax + ticks.xStep,
      ticks.xStep
    );
    const yTicks = d3.range(
      ticks.yStart,
      ranges.yMax + ticks.yStep,
      ticks.yStep
    );

    svg
      .selectAll(".x-axis")
      .data([null])
      .join("g")
      .attr("class", "x-axis")
      .attr("transform", `translate(0,${height - margin.bottom})`)
      .transition(t)
      .call(d3.axisBottom(xPositionScale).tickValues(xTicks));

    svg
      .selectAll(".y-axis-label")
      .data([null])
      .join(
        (enter) =>
          enter
            .append("text")
            .text(ranges.yLabel)
            .attr("class", "y-axis-label")
            .attr("transform", "rotate(-90)")
            .attr("x", -(height - margin.top - margin.bottom) / 2 - margin.top)
            .attr("y", "1em")
            .style("text-anchor", "middle")
            .call((enter) => enter.transition(t)),
        (update) =>
          update.call((enter) => enter.text(ranges.yLabel).transition(t))
      );

    svg
      .selectAll(".y-axis")
      .data([null])
      .join("g")
      .attr("class", "y-axis")
      .attr("transform", `translate(${margin.left},0)`)
      .call(d3.axisLeft(yPositionScale).tickValues(yTicks));

    svg
      .selectAll(".x-axis-label")
      .data([null])
      .join(
        (enter) =>
          enter
            .append("text")
            .text(ranges.xLabel)
            .attr("class", "x-axis-label")
            .attr("x", (width - margin.left - margin.right) / 2 + margin.left)
            .attr("y", height - 3)
            .style("text-anchor", "middle")
            .call((enter) => enter.transition(t)),
        (update) =>
          update.call((enter) => enter.text(ranges.xLabel).transition(t))
      );
  };

  my.aspect = function (_) {
    return arguments.length ? ((aspect = +_), my) : aspect;
  };

  my.height = function (_) {
    return arguments.length ? ((height = +_), my) : height;
  };

  my.width = function (_) {
    return arguments.length ? ((width = +_), my) : width;
  };

  my.data = function (_) {
    return arguments.length ? ((data = _), my) : data;
  };

  my.margin = function (_) {
    return arguments.length ? ((margin = _), my) : margin;
  };

  my.radius = function (_) {
    return arguments.length ? ((radius = +_), my) : radius;
  };

  my.xValue = function (_) {
    return arguments.length ? ((xValue = _), my) : xValue;
  };

  my.yValue = function (_) {
    return arguments.length ? ((yValue = _), my) : yValue;
  };

  my.groupBy = function (_) {
    return arguments.length ? ((groupBy = _), my) : groupBy;
  };

  my.ranges = function (_) {
    return arguments.length ? ((ranges = _), my) : ranges;
  };

  my.ticks = function (_) {
    return arguments.length ? ((ticks = _), my) : ticks;
  };

  return my;
};
