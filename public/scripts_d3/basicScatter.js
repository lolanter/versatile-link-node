//import  { csv, select } from 'd3';
import * as d3 from "https://unpkg.com/d3?module";
import { scatterPlot } from "./scatterPlot.js";

const csvUrl =
  "https://gist.githubusercontent.com/curran/a08a1080b88344b0c8a7/raw/0e7a9b0a5d22642a06d3d5b9bcbad9890c8ee534/iris.csv";

const parseRow = (d) => {
  d.sepal_length = +d.sepal_length;
  d.sepal_width = +d.sepal_width;
  d.petal_length = +d.petal_length;
  d.petal_width = +d.petal_width;
  return d;
};

const width = 800;
const height = 500;
const selection = d3
  .select("body")
  .append("svg")
  .attr("width", width)
  .attr("height", height);

const main = async () => {
  const data = csv(csvUrl, parseRow);
  selection.call(
    scatterPlot()
      .width(width)
      .height(height)
      .data(data)
      .xValue((d) => d.petal_length)
      .yValue((d) => d.sepal_length)
      .margin({
        top: 20,
        right: 20,
        bottom: 20,
        left: 50,
      })
      .radius(3)
  );
};
main();
