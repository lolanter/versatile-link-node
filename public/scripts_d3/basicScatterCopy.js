const { csv, select, scaleLinear, extent, axisLeft, axisBottom } = d3;

// Tweakables
const csvUrl =
  "https://gist.githubusercontent.com/curran/a08a1080b88344b0c8a7/raw/0e7a9b0a5d22642a06d3d5b9bcbad9890c8ee534/iris.csv";

const parseRow = (d) => {
  d.sepal_length = +d.sepal_length;
  d.sepal_width = +d.sepal_width;
  d.petal_length = +d.petal_length;
  d.petal_width = +d.petal_width;
  return d;
};

// Functions that returns to petal lengths and sepal lengths
const xValue = (d) => d.petal_length;
const yValue = (d) => d.sepal_width;

const margin = { top: 20, right: 20, bottom: 20, left: 50 };
const radius = 5;

const width = window.innerWidth;
const height = window.innerHeight;
const svg = select("body")
  .append("svg")
  .attr("width", width)
  .attr("height", height);

// Generic
const main = async () => {
  const data = await csv(csvUrl, parseRow);

  // Method chaining, same as first const x = scaleLinear(); x.domain(extent(data, xValue)) etc...
  const xScale = scaleLinear()
    //.domain([d3.min(data, xValue), d3.max(data, xValue)]);
    .domain([0, d3.max(data, xValue)])
    .range([margin.left, width - margin.right]);

  const yScale = scaleLinear()
    .domain(extent(data, yValue))
    .range([height - margin.bottom, 0 + margin.top]);

  const marks = data.map((d) => ({
    x: xScale(xValue(d)),
    y: yScale(yValue(d)),
  }));

  svg
    .selectAll("circle")
    .data(marks)
    .join("circle")
    .attr("cx", (d) => d.x)
    .attr("cy", (d) => d.y)
    .attr("r", radius);

  svg
    .append("g")
    .attr("transform", `translate(0,${height - margin.bottom})`)
    .call(axisBottom(xScale));

  svg
    .append("g")
    .attr("transform", `translate(${margin.left},0)`)
    .call(axisLeft(yScale));
  // or:
  //const yAxis = axisLeft(yScale); // axis as a standalone thing
  //const yAxisG = svg  // group element as a standalone thing
  //    .append('g')
  //    .attr('transform', `translate(${margin.left},0)`)
  //yAxis(yAxisG); // yAxis is a function that expects to be called with D3 selection of a group element as an input
  // so it could be called also like this:
  // yAxisG.call(yAxis)
};

main();
