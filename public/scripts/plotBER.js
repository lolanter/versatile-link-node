function plotBER(berdata, chartCanvas) {
  const BERplot = document.getElementById(chartCanvas);

  const borderWidth = 1;

  let xMin = -18;
  let xMax = -13;
  let annotationText1 = [
    `${berdata.sens !== null ? berdata.sens.toFixed(1) : ""} uW`,
  ];
  let annotationText2 = [
    `${berdata.sensqa !== null ? berdata.sensqa.toFixed(1) : ""} dBm`,
  ];
  let xTitle = "Optical Modulation Amplitude [dBm]";
  let xUnit = "dBm";

  if (chartCanvas == "BERplotWatts") {
    berdata.berCurve = berdata.berCurveWatts;
    berdata.berCurveSens = berdata.berCurveSensWatts;
    berdata.sens = berdata.sensWatts;
    berdata.sensqa = berdata.sensqaWatts;

    xMin = 10;
    xMax = 50;
        
    annotationText1 = [
      `${berdata.sens !== null ? berdata.sens.toFixed(1) : ""} uW`,
    ];
    annotationText2 = [
      `${berdata.sensqa !== null ? berdata.sensqa.toFixed(1) : ""} uW`,
    ];

    xTitle = "Optical Modulation Amplitude [uW]";
    xUnit = "uW";
  }

  // Data block:
  const data = {
    datasets: [
      {
        label: "Measured",
        borderColor: "red",
        backgroundColor: "red",
        data: berdata.berCurve,
        showLine: true,
        borderWidth: borderWidth,
      },
      {
        label: "Extrapolation",
        borderColor: "blue",
        backgroundColor: "blue",
        data: berdata.berCurveSens,
        showLine: true,
        borderWidth: borderWidth,
      },
    ],
  };

  // Plugins:
  const plugins = {
    annotation: {
      annotations: {
        label1: {
          type: "label",
          xValue: 1.05 * berdata.sens,
          yValue: berdata.sensqa !== null ? 3e-10 : 3e-12,
          // backgroundColor: "rgba(256,256,256)",
          content: annotationText1,
        },
        label2: {
          type: "label",
          xValue: 1.05 * berdata.sensqa,
          yValue: 3e-12,
          //backgroundColor: "rgba(256,256,256)",
          content: annotationText2,
        },
      },
    },
    tooltip: {
      callbacks: {
        label: function (context) {
          let label = " ";
          if (context.parsed.y !== null) {
            label += context.parsed.y.toExponential(1) + " @ ";
            label += context.parsed.x.toFixed(1) + " " + xUnit;
          }
          return label;
        },
      },
    },
  };

  // Options block:
  const options = {
    aspectRatio: 1.5,
    animation: {
      duration: 0,
    },
    hover: {
      mode: "nearest",
      intersect: true,
    },
    scales: {
      y: {
        suggestedMin: 1e-12,
        suggestedMax: 1e-3,
        type: "logarithmic",
        title: {
          display: true,
          text: "BER",
        },
        ticks: {
          min: 1e-12,
          max: 1e-3,
          callback: (val) => val.toExponential(),
        },
        afterBuildTicks: function (chart) {
          chart.ticks = [
            1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8, 1e-9, 1e-10, 1e-11, 1e-12,
          ].map((v) => ({ value: v }));
        },
      },
      x: {
        suggestedMin: xMin,
        suggestedMax: xMax,
        title: {
          display: true,
          text: xTitle,
        },
      },
    },
    plugins,
  };

  const chart = new Chart(BERplot, {
    type: "scatter",
    data,
    options,
  });
}
