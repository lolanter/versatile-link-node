function plotMonitoring(
  paramdata,
  chartCanvas,
  sampleId,
  xMin,
  xMax,
  paramNames
) {
  const plot = document.getElementById(chartCanvas);

  const colors = [
    "red",
    "blue",
    "green",
    "orange",
    "fuchsia",
    "aqua",
    "lime",
    "yellow",
    "coral",
    "darkcyan",
    "darkseagreen",
    "sandybrown",
  ];

  const borderWidth = 0.1;

  // Data block:
  let datasets = [];
  for (let ch = 0; ch < paramdata.length; ch++) {
    datasets.push({
      label: paramNames[ch],
      borderColor: colors[ch % colors.length],
      backgroundColor: colors[ch % colors.length],
      data: paramdata[ch],
      pointRadius: 0,
      showLine: true,
      borderWidth: borderWidth,
      stepped: true,
      fill: true,
    });
  }

  const data = {
    datasets,
  };

  // Plugins:
  const plugins = {
    annotation: {},
  };

  // Options block:
  const options = {
    _sampleId: sampleId,
    plugins,
    scales: {
      x: {
        min: xMin,
        max: xMax,
      },
      y: {
        beginAtZero: true,
        grace: 10,
        stacked: true,
      },
    },
  };

  const chart = new Chart(plot, {
    type: "scatter",
    data: data,
    options,
  });

  return chart;
}

// UTILITY FUNCTIONS
function drawLimits(sampleId, min, max) {
  const chart = Object.values(Chart.instances).find(
    (c) => c.options._sampleId === sampleId
  );

  let annotation = {
    annotations: {
      lineMin: {
        type: "line",
        xMin: min,
        xMax: min,
        borderColor: "black",
        borderWidth: 1,
      },
      lineMax: {
        type: "line",
        xMin: max,
        xMax: max,
        borderColor: "black",
        borderWidth: 1,
      },
    },
  };

  chart.options.plugins.annotation = annotation;

  chart.update();
}

function toggleData(id, sampleId) {
  const chart = Object.values(Chart.instances).find(
    (c) => c.options._sampleId === sampleId
  );

  //  document.getElementById(sampleId.replace("Id", "Avg")).value = 0;
  //  document.getElementById(
  //    sampleId.replace("Id", "Avg") + "Value"
  //  ).textContent = 0;

  const colors = [
    "red",
    "blue",
    "green",
    "orange",
    "fuchsia",
    "aqua",
    "lime",
    "yellow",
    "coral",
    "darkcyan",
    "darkseagreen",
    "sandybrown",
  ];

  const dataId = sampleId.replace("Id", "Data");
  const data = JSON.parse(document.getElementById(dataId).innerHTML);

  let paramdata = [];
  let paramNames = [];
  if (id) {
    paramdata = data.channelData;
    paramNames = data.channelNames;
  } else {
    paramdata = data.panelData;
    paramNames = data.panelNames;
  }

  let datasets = [];
  for (let i = 0; i < paramdata.length; i++) {
    datasets.push({
      label: paramNames[i],
      borderColor: colors[i % colors.length],
      backgroundColor: colors[i % colors.length],
      data: paramdata[i],
      pointRadius: 0,
      borderWidth: 0.1,
      showLine: true,
      stepped: true,
      fill: true,
    });
    chart.data.datasets = datasets;

    chart.update();
  }
}
