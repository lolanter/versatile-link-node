
function plotOrders(plotData, chartCanvas, i) {
  const plot = document.getElementById(chartCanvas);

  const borderWidth = 2;
  const pointRadius = 2;

  const currentDate = new Date()

  let endDates = []
  endDates.push(plotData.plotRequest[i][plotData.plotRequest[i].length-1].x)
  endDates.push(plotData.plotOrder[i][plotData.plotOrder[i].length-1].x)
  endDates.push(plotData.plotPendingOrder[i][plotData.plotPendingOrder[i].length-1].x)
  endDates.push(plotData.plotReception[i][plotData.plotReception[i].length-1].x)
  endDates.push(plotData.plotEta[i][plotData.plotEta[i].length-1].x)
  endDates.push(plotData.plotDelivery[i][plotData.plotDelivery[i].length-1].x)
  endDates.sort()
  const dateArray = endDates[endDates.length - 1].split('-')

  let year = parseInt(dateArray[0])
  if(parseInt(dateArray[1])+2 > 12){
    year += 1
  }

  let month = (parseInt(dateArray[1])+2) % 12
  if(month == 0){
    month = 1
  }
  if(month < 10){
    month = '0' + month
  }
 
  const maxDate = year+'-'+month+'-01'

  // Data block:
  let datasets = [];
  datasets.push({
    label: "Request",
    borderColor: "red",
    backgroundColor: "red",
    data: plotData.plotRequest[i],
    pointRadius: pointRadius,
    showLine: true,
    borderWidth: borderWidth,
    stepped: false,
    fill: false,
  });
  datasets.push({
    label: "Order",
    borderColor: "blue",
    backgroundColor: "blue",
    data: plotData.plotOrder[i],
    pointRadius: pointRadius,
    showLine: true,
    borderWidth: borderWidth,
    stepped: true,
    fill: false,
  });
  datasets.push({
    label: "Pending order",
    borderColor: "blue",
    backgroundColor: "blue",
    data: plotData.plotPendingOrder[i],
    pointRadius: pointRadius,
    showLine: true,
    borderWidth: borderWidth,
    stepped: true,
    fill: false,
    borderDash: [10,5]
  });
  datasets.push({
    label: "Reception",
    borderColor: "orange",
    backgroundColor: "orange",
    data: plotData.plotReception[i],
    pointRadius: pointRadius,
    showLine: true,
    borderWidth: borderWidth,
    stepped: true,
    fill: false,
  });
  datasets.push({
    label: "Estimated reception",
    borderColor: "orange",
    backgroundColor: "orange",
    data: plotData.plotEta[i],
    pointRadius: pointRadius,
    showLine: true,
    borderWidth: borderWidth,
    stepped: true,
    fill: false,
    borderDash: [10,5]
  });
  datasets.push({
    label: "Delivery to user",
    borderColor: "green",
    backgroundColor: "green",
    data: plotData.plotDelivery[i],
    pointRadius: pointRadius,
    showLine: true,
    borderWidth: borderWidth,
    stepped: true,
    fill: false,
  });

  const data = {
    datasets,
  };

  // Plugins:
  const plugins = {
    tooltip: {
      callbacks: {
        label: function (tooltipItem) {
          return `${tooltipItem.dataset.label}  ${tooltipItem.raw.x.split('T')[0]}: ${tooltipItem.dataIndex != 0 ? tooltipItem.dataset.data[tooltipItem.dataIndex].y-tooltipItem.dataset.data[tooltipItem.dataIndex-1].y : tooltipItem.dataset.data[tooltipItem.dataIndex].y}`; 
        }
      }
    },
    annotation: {
      annotations: {
        today: {
          type: 'line',
          drawTime: 'beforeDatasetsDraw',
          xMin: currentDate,
          xMax: currentDate,
        },
        requested: {
          type: 'line',
          drawTime: 'beforeDatasetsDraw',
          borderWidth: 20,
          borderColor: '#FFCCCB',
          opacity: 0.5,
          xMin: maxDate,
          xMax: maxDate,
          yMin: 0,
          yMax: plotData.plotRequest[i][plotData.plotRequest[i].length - 1].y,
        },
        ordered: {
          type: 'line',
          drawTime: 'beforeDatasetsDraw',
          borderWidth: 20,
          borderColor: '#90d5ff',
          opacity: 0.5,
          xMin: maxDate,
          xMax: maxDate,
          yMin: 0,
          yMax: plotData.plotOrder[i][plotData.plotOrder[i].length - 1].y,
        },
        received: {
          type: 'line',
          drawTime: 'beforeDatasetsDraw',
          borderWidth: 20,
          borderColor: '#FFD580',
          opacity: 0.5,
          xMin: maxDate,
          xMax: maxDate,
          yMin: 0,
          yMax: plotData.plotReception[i][plotData.plotReception[i].length - 1].y,
        },
        delivered: {
          type: 'line',
          drawTime: 'beforeDatasetsDraw',
          borderWidth: 20,
          borderColor: '#90EE90',
          xMin: maxDate,
          xMax: maxDate,
          yMin: 0,
          yMax: plotData.plotDelivery[i][plotData.plotDelivery[i].length - 1].y,
        },
        orderedLabel: {
          type: 'label',
          xAdjust: -25,
          xValue: maxDate,
          yValue: plotData.plotOrder[i][plotData.plotOrder[i].length - 1].y,
          content: [Math.round(plotData.plotOrder[i][plotData.plotOrder[i].length - 1].y/plotData.plotRequest[i][plotData.plotRequest[i].length - 1].y*100)+'%'],
        },
        deliveredLabel: {
          type: 'label',
          xAdjust: -25,
          xValue: maxDate,
          yValue: plotData.plotDelivery[i][plotData.plotDelivery[i].length - 1].y,
          content: [Math.round(plotData.plotDelivery[i][plotData.plotDelivery[i].length - 1].y/plotData.plotRequest[i][plotData.plotRequest[i].length - 1].y*100)+'%'],
        }
      }
    }
  }
  // Options block:
  const options = {
    aspectRatio: 1.5,
    plugins,
    animation: {
      duration: 0
    },
    responsive: true,
    scales: {
      x: {
        type: "time",
        time: {
          unit: 'month',
        },
        max: maxDate
      },
      y: {
        beginAtZero: true,
      },
    },
  };

  const chart = new Chart(plot, {
    type: "scatter",
    data: data,
    options
  });

  return chart;
}


