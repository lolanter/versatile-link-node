function plotEYE(eyedata, chartCanvas, channel) {
  const EYEplot = document.getElementById(chartCanvas);

  let color = 'red';
  if(channel === 2){
    color = 'blue';
  } else if (channel === 3){
    color = 'green';
  } else if (channel === 4){
    color = 'orange';
  } 

  channel--;

  const borderWidth = 1;

  let xTitle = "Time [ps]";

  // Data block:
  const data = {
    datasets: [
      {
        label: "RSSI",
        borderColor: color,
        backgroundColor: color,
        data: eyedata[channel].topTrace,
        showLine: true,
        borderWidth: borderWidth,
      },
      {
        label: "RSSI",
        borderColor: color,
        backgroundColor: color,
        data: eyedata[channel].bottomTrace,
        showLine: true,
        borderWidth: borderWidth,
      },
      {
        label: "RSSI",
        borderColor: color,
        backgroundColor: color,
        data: eyedata[channel].oneTrace,
        showLine: true,
        borderWidth: borderWidth,
      },
      {
        label: "RSSI",
        borderColor: color,
        backgroundColor: color,
        data: eyedata[channel].zeroTrace,
        showLine: true,
        borderWidth: borderWidth,
      },
      {
        label: "RSSI",
        borderColor: "gray",
        backgroundColor: "gray",
        data: eyedata[channel].eyeOpeningTrace,
        showLine: true,
        borderWidth: borderWidth,
      },
      {
        label: "RSSI",
        borderColor: "gray",
        backgroundColor: "gray",
        data: eyedata[channel].eyeOpeningLeftTrace,
        showLine: true,
        borderWidth: borderWidth,
      },
      {
        label: "RSSI",
        borderColor: "gray",
        backgroundColor: "gray",
        data: eyedata[channel].eyeOpeningRightTrace,
        showLine: true,
        borderWidth: borderWidth,
      },
    ],
  };

  // Plugins:
  const plugins = {
    legend: {
      display: false
    }
  };

  // Options block:
  const options = {
    aspectRatio: 1.5,
    animation: {
      duration: 0,
    },
    hover: {
      mode: "nearest",
      intersect: true,
    },
    scales: {
      y: {
        title: {
          display: true,
          text: "Optical Power [mW]",
        },
        min: 0,
        max: 2.5,
      },
      x: {
        title: {
          display: true,
          text: xTitle,
        },
      },
    },
    elements: {
      point:{
        radius: 0
      }
    },
    plugins,
  };

  const chart = new Chart(EYEplot, {
    type: "scatter",
    data,
    options,
  });
}
