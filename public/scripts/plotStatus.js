function plotStatus(passfaildata, chartCanvas, sampleId) {
  const plot = document.getElementById(chartCanvas);

  const borderWidth = 0.1;

  // Data block:
  let datasets = [];
  datasets.push({
    label: "Fail",
    borderColor: "red",
    backgroundColor: "red",
    data: passfaildata.fail,
    pointRadius: 0,
    showLine: true,
    borderWidth: borderWidth,
    stepped: true,
    fill: true,
  });
  datasets.push({
    label: "Pass",
    borderColor: "green",
    backgroundColor: "green",
    data: passfaildata.pass,
    pointRadius: 0,
    showLine: true,
    borderWidth: borderWidth,
    stepped: true,
    fill: true,
  });

  const data = {
    datasets,
  };

  // Plugins:
  const plugins = {
    tooltip: {
      enabled: true,
      callbacks: {
        title: function (context) {
          const titleArray = context[0].label.split(" ");
          return titleArray[0] + " " + parseInt(titleArray[2]);
        },
      },
    },
  };

  // Options block:
  const options = {
    _sampleId: sampleId,
    plugins,
    responsive: true,
    scales: {
      x: {
        type: "time",
        time: {
          unit: passfaildata.timeUnit,
          isoWeekday: true,
        },
      },
      y: {
        grace: 5,
      },
    },
  };

  const chart = new Chart(plot, {
    type: "bar",
    data: data,
    options,
  });

  return chart;
}

function plotRunningTotal(running, chartCanvas, sampleId) {
  const plot = document.getElementById(chartCanvas);

  const borderWidth = 2;

  // Data block:
  let datasets = [];
  datasets.push({
    label: "Fail",
    borderColor: "red",
    backgroundColor: "red",
    data: running.failRunning,
    pointRadius: 0,
    showLine: true,
    borderWidth: borderWidth,
    stepped: true,
    fill: false,
    yAxisID: 'y',
  });
  datasets.push({
    label: "Pass",
    borderColor: "green",
    backgroundColor: "green",
    data: running.passRunning,
    pointRadius: 0,
    showLine: true,
    borderWidth: borderWidth,
    stepped: true,
    fill: false,
    yAxisID: 'y',
  });
  datasets.push({
    label: "Yield previous 30 days",
    borderColor: "black",
    backgroundColor: "black",
    data: running.yieldRunning,
    pointRadius: 0,
    showLine: true,
    borderWidth: borderWidth,
    stepped: true,
    fill: false,
    yAxisID: 'y1',
  });

  const data = {
    datasets,
  };

  // Plugins:
  const plugins = {
    tooltip: {
      enabled: true,
      callbacks: {
        label: function(context) {
            let label
            if (context.parsed.y !== null) {
                label = ' ' + Math.round(context.parsed.y*10)/10;
            }
           
            return label;
        }
      }
    },
  };

  // Options block:
  const options = {
    _sampleId: sampleId,
    plugins,
    responsive: true,
    scales: {
      x: {
        type: "time",
        time: {
          unit: 'month'
        }
      },
      y: {
        beginAtZero: true,
        grace: 5,
        stacked: true,
        grid: {
          drawOnChartArea: false, // only want the grid lines for one axis to show up
        },
      },
      y1: {
        min: 70,
        max: 100,
        type: 'linear',
        display: true,
        position: 'right',

        // grid line settings
        //grid: {
        //  drawOnChartArea: false, // only want the grid lines for one axis to show up
        //},
      },
    },
  };

  const chart = new Chart(plot, {
    type: "scatter",
    data: data,
    options,
  });

  return chart;
}

// UTILITY FUNCTIONS

// Returns the ISO week of the date.
function getWeek(date) {
  date.setHours(0, 0, 0, 0);
  // Thursday in current week decides the year.
  date.setDate(date.getDate() + 3 - ((date.getDay() + 6) % 7));
  // January 4 is always in week 1.
  var week1 = new Date(date.getFullYear(), 0, 4);
  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
  return (
    1 +
    Math.round(
      ((date.getTime() - week1.getTime()) / 86400000 -
        3 +
        ((week1.getDay() + 6) % 7)) /
        7
    )
  );
}

function limitRange(min, sampleId, sampleIdRunning) {
  let minInputValue = document.getElementById("minDateInput").value;
  let maxInputValue = document.getElementById("maxDateInput").value;

  if (maxInputValue === "") {
    maxInputValue = new Date().toISOString().split("T")[0];
    document.getElementById("maxDateInput").value = maxInputValue;
  }

  if (min === 1 && maxInputValue < minInputValue) {
    document.getElementById("maxDateInput").value =
      document.getElementById("minDateInput").value;
  }
  if (min === 0 && minInputValue > maxInputValue) {
    document.getElementById("minDateInput").value =
      document.getElementById("maxDateInput").value;
  }

  minInputValue = document.getElementById("minDateInput").value;
  maxInputValue = document.getElementById("maxDateInput").value;

  let difference =
    new Date(maxInputValue).getTime() - new Date(minInputValue).getTime();
  let totalDays = Math.ceil(difference / (1000 * 3600 * 24));

  limitRangeStatus(sampleId, maxInputValue, minInputValue, totalDays);

  limitRangeRunning(sampleIdRunning, maxInputValue, minInputValue, totalDays);
}

function limitRangeStatus(sampleId, maxInputValue, minInputValue, totalDays) {
  const data = JSON.parse(document.getElementById("data").innerHTML);

  let timeUnit;
  let titleFunction;
  let productionTitle;
  if (totalDays <= 30) {
    timeUnit = "day";
    productionTitle = "Production daily";
    titleFunction = function (context) {
      const titleArray = context[0].label.split(" ");
      return (
        titleArray[0] + " " + titleArray[1] + " " + parseInt(titleArray[2])
      );
    };
  } else if (totalDays < 120) {
    timeUnit = "week";
    productionTitle = "Production weekly";
    titleFunction = function (context) {
      const titleArray = context[0].label.split(" ");
      return (
        "Week of " +
        titleArray[0] +
        " " +
        titleArray[1] +
        " " +
        parseInt(titleArray[2])
      );
    };
  } else {
    timeUnit = "month";
    productionTitle = "Production monthly";
    titleFunction = function (context) {
      const titleArray = context[0].label.split(" ");
      return titleArray[0] + " " + parseInt(titleArray[2]);
    };
  }

  document.getElementById("productionTitle").innerHTML = productionTitle;

  const passGrouped = groupData(data.passfail.pass, timeUnit);
  const failGrouped = groupData(data.passfail.fail, timeUnit);

  const chart = Object.values(Chart.instances).find(
    (c) => c.options._sampleId === sampleId
  );

  // Plugins:
  const plugins = {
    tooltip: {
      enabled: true,
      callbacks: {
        title: titleFunction,
      },
    },
  };

  chart.options.plugins = plugins;
  chart.options.scales.x.time.unit = timeUnit;
  chart.options.scales.x.min = minInputValue;
  chart.options.scales.x.max = maxInputValue;
  chart.data.datasets[0].data = failGrouped;
  chart.data.datasets[1].data = passGrouped;

  chart.update();
}

function limitRangeRunning(sampleId, maxInputValue, minInputValue, totalDays) {
  const data = JSON.parse(document.getElementById("data").innerHTML);

  let timeUnit;
  if (totalDays <= 30) {
    timeUnit = "day";
  } else if (totalDays < 120) {
    timeUnit = "week";
  } else {
    timeUnit = "month";
  }

  let failRunning = data.running.failRunning;
  let passRunning = data.running.passRunning;
  let yieldRunning = data.running.yieldRunning;

  let failAtStart = 0;
  let passAtStart = 0;
  for (let i = 0; i < failRunning.length; i++) {
    if (new Date(failRunning[i].x) < new Date(minInputValue)) {
      failAtStart = failRunning[i].y;
      passAtStart = passRunning[i].y;
    }
  }

  for (let i = 0; i < failRunning.length; i++) {
    failRunning[i].y = failRunning[i].y - failAtStart;
    passRunning[i].y = passRunning[i].y - passAtStart;
  //  yieldRunning[i].y = failRunning[i].y + passRunning[i].y === 0 ? 100 : passRunning[i].y/(failRunning[i].y + passRunning[i].y)*100;
  }
  const chart = Object.values(Chart.instances).find(
    (c) => c.options._sampleId === sampleId
  );

  chart.data.datasets[0].data = failRunning;
  chart.data.datasets[1].data = passRunning;
  chart.data.datasets[2].data = yieldRunning;
  chart.options.scales.x.min = minInputValue;
  chart.options.scales.x.max = maxInputValue;
  chart.options.scales.x.time.unit = timeUnit;

  chart.update();
}

function groupData(dataArray, timeUnit) {
  for (let i = 0; i < dataArray.length - 1; i++) {
    if (timeUnit === "month") {
      const cat =
        dataArray[i].x.split("-")[0] + "-" + dataArray[i].x.split("-")[1];
      const catNext =
        dataArray[i + 1].x.split("-")[0] +
        "-" +
        dataArray[i + 1].x.split("-")[1];
      if (cat === catNext) {
        dataArray[i].x =
          dataArray[i].x.split("-")[0] +
          "-" +
          dataArray[i].x.split("-")[1] +
          "-01";
        dataArray[i].y = dataArray[i].y + dataArray[i + 1].y;
        dataArray.splice(i + 1, 1);
        i--;
      } else {
        dataArray[i].x =
          dataArray[i].x.split("-")[0] +
          "-" +
          dataArray[i].x.split("-")[1] +
          "-01";
        dataArray[i].y = dataArray[i].y;
      }
    } else if (timeUnit === "week") {
      let prevMonday = new Date(dataArray[i].x);

      prevMonday.setDate(
        prevMonday.getDate() - ((prevMonday.getDay() + 6) % 7)
      );

      const cat =
        dataArray[i].x.split("-")[0] + "-" + getWeek(new Date(dataArray[i].x));
      const catNext =
        dataArray[i + 1].x.split("-")[0] +
        "-" +
        getWeek(new Date(dataArray[i + 1].x));
      if (cat === catNext) {
        dataArray[i].y = dataArray[i].y + dataArray[i + 1].y;
        dataArray.splice(i + 1, 1);
        i--;
      } else {
        dataArray[i].x = prevMonday;
        dataArray[i].y = dataArray[i].y;
      }
    } else if (timeUnit === "day") {
      const cat =
        dataArray[i].x.split("-")[0] +
        "-" +
        dataArray[i].x.split("-")[1] +
        "-" +
        dataArray[i].x.split("-")[2];
      const catNext =
        dataArray[i + 1].x.split("-")[0] +
        "-" +
        dataArray[i + 1].x.split("-")[1] +
        "-" +
        dataArray[i + 1].x.split("-")[2];
      if (cat === catNext) {
        dataArray[i].x =
          dataArray[i].x.split("-")[0] +
          "-" +
          dataArray[i].x.split("-")[1] +
          "-" +
          dataArray[i].x.split("-")[2];
        dataArray[i].y = dataArray[i].y + dataArray[i + 1].y;
        dataArray.splice(i + 1, 1);
        i--;
      } else {
        dataArray[i].x =
          dataArray[i].x.split("-")[0] +
          "-" +
          dataArray[i].x.split("-")[1] +
          "-" +
          dataArray[i].x.split("-")[2];
        dataArray[i].y = dataArray[i].y;
      }
    }
  }

  return dataArray;
}
