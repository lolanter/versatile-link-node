function plotLI(lidata, chartCanvas) {
  const LIplot = document.getElementById(chartCanvas);

  const borderWidth = 1;
  let tension = 0.5;
  let cubicInterpolationMode = "monotone";

  let ch1data = lidata[0].dataBias;
  let ch2data = lidata[1].dataBias;
  let ch3data = lidata[2].dataBias;
  let ch4data = lidata[3].dataBias;

  let maxPower = Math.max(ch1data[ch1data.length - 1].y, ch2data[ch2data.length - 1].y, ch3data[ch3data.length - 1].y,ch4data[ch4data.length - 1].y);

  let maxPopt =
    Math.ceil(
      Math.max(
        lidata[0].PoptTyp,
        lidata[1].PoptTyp,
        lidata[2].PoptTyp,
        lidata[3].PoptTyp
      ) * 10000
    ) / 10;
  let minPopt =
    Math.floor(
      Math.min(
        lidata[0].PoptTyp,
        lidata[1].PoptTyp,
        lidata[2].PoptTyp,
        lidata[3].PoptTyp
      ) * 10000
    ) / 10;

  let xTitle = "Bias Current [mA]";
  let xMin = 0;
  let xMax = 16;
  let yMin = 0;
  let yMax = maxPower > 2.5 ? 3.0 : 2.5;
  let ticksArray = [0, 2, 4, 6, 8, 10, 12, 14, 16];

  let annotationX = 100;
  let annotationY = 100;
  let annotationText = [];
  
  let tooltipUnit = " mW";
  let tooltipDecimals = 2;

  if (chartCanvas === "LIplotIth") {
    xMin = 0;
    xMax = 2;
    yMin = 0;
    yMax = 0.3;

    ticksArray = [0.4, 0.8, 1.2, 1.6, 2];

    annotationX = 0.2 * (xMax - xMin) + xMin;
    annotationY = 0.8 * yMax;
    annotationText = [
      `Ch1: ${(lidata[0].Ith * 1000).toFixed(2)} mA`,
      `Ch2: ${(lidata[1].Ith * 1000).toFixed(2)} mA`,
      `Ch3: ${(lidata[2].Ith * 1000).toFixed(2)} mA`,
      `Ch4: ${(lidata[3].Ith * 1000).toFixed(2)} mA`,
    ];
  } else if (chartCanvas === "LIplotPopt") {
    xMin = 5;
    xMax = 7;
    yMin = minPopt * 0.9;
    yMax = maxPopt * 1.1;

    ticksArray = [5, 5.5, 6, 6.5, 7];

    annotationX = 0.2 * (xMax - xMin) + xMin;
    annotationY = 0.8 * (yMax - yMin) + yMin;
    annotationText = [
      `Ch1: ${(lidata[0].PoptTyp * 1000).toFixed(2)} mW`,
      `Ch2: ${(lidata[1].PoptTyp * 1000).toFixed(2)} mW`,
      `Ch3: ${(lidata[2].PoptTyp * 1000).toFixed(2)} mW`,
      `Ch4: ${(lidata[3].PoptTyp * 1000).toFixed(2)} mW`,
    ];

    tension = 0;
    cubicInterpolationMode = "default";
  } else if (chartCanvas === "LIplotSetting") {
    ch1data = lidata[0].dataSet;
    ch2data = lidata[1].dataSet;
    ch3data = lidata[2].dataSet;
    ch4data = lidata[3].dataSet;

    xTitle = "Bias Setting";
    xMin = 0;
    xMax = 128;
    ticksArray = [0, 16, 32, 48, 64, 80, 96, 112, 128];
  
    tooltipUnit = "";
    tooltipDecimals = 0;
  }

  // Data block:
  const data = {
    datasets: [
      {
        label: "Ch1",
        borderColor: "red",
        backgroundColor: "red",
        data: ch1data,
        showLine: true,
        borderWidth: borderWidth,
        cubicInterpolationMode: cubicInterpolationMode,
        tension: tension,
      },
      {
        label: "Ch2",
        borderColor: "blue",
        backgroundColor: "blue",
        data: ch2data,
        showLine: true,
        borderWidth: borderWidth,
        cubicInterpolationMode: cubicInterpolationMode,
        tension: tension,
      },
      {
        label: "Ch3",
        borderColor: "green",
        backgroundColor: "green",
        data: ch3data,
        showLine: true,
        borderWidth: borderWidth,
        cubicInterpolationMode: cubicInterpolationMode,
        tension: tension,
      },
      {
        label: "Ch4",
        borderColor: "orange",
        backgroundColor: "orange",
        data: ch4data,
        showLine: true,
        borderWidth: borderWidth,
        cubicInterpolationMode: cubicInterpolationMode,
        tension: tension,
      },
    ],
  };

  // Plugins:
  const plugins = {
    annotation: {
      annotations: {
        label1: {
          type: "label",
          xValue: annotationX,
          yValue: annotationY,
          //backgroundColor: "white",
          content: annotationText,
        },
      },
    },
    tooltip: {
      callbacks: {
        label: function (context) {
          let label = " ";
          if (context.parsed.y !== null) {
            label += context.parsed.y.toFixed(2) + " mW @ ";
            label += context.parsed.x.toFixed(tooltipDecimals) + tooltipUnit;
          }
          return label;
        },
      },
    },
  };

  // Options block:
  const options = {
    aspectRatio: 1.5,
    animation: {
      duration: 0,
    },
    hover: {
      mode: "nearest",
      intersect: true,
    },
    scales: {
      x: {
        title: {
          display: true,
          text: xTitle,
        },
        min: xMin,
        max: xMax,
        afterBuildTicks: function (chart) {
          chart.ticks = ticksArray.map((v) => ({ value: v }));
        },
      },
      y: {
        min: yMin,
        max: yMax,
        title: {
          display: true,
          text: "Optical Power [mW]",
        },
      },
    },
    plugins,
  };

  const chart = new Chart(LIplot, {
    type: "scatter",
    data,
    options,
    plugins: ["chartjs-plugin-annotation"],
  });
}
