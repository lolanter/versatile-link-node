function plotRSSI(rssidata, chartCanvas) {
    
  const RSSIplot = document.getElementById(chartCanvas);

  const borderWidth = 1;

  let annotationText1 = [`${rssidata.resp.toFixed(2)} A/W`];
  let xTitle = "Optical Input Power [mW]";

 
  // Data block:
  const data = {
    datasets: [
      {
        label: "RSSI",
        borderColor: "red",
        backgroundColor: "red",
        data: rssidata.data,
        showLine: true,
        borderWidth: borderWidth,
      }
    ],
  };

  // Plugins:
  const plugins = {
    annotation: {
      annotations: {
        label1: {
          type: "label",
          xValue: rssidata.data[1].x-0.05,
          yValue: rssidata.data[1].y+0.02,
          content: annotationText1,
        }
      },
    },
    tooltip: {
      callbacks: {
        label: function (context) {
          let label = " ";
          if (context.parsed.y !== null) {
            label += context.parsed.y.toFixed(2) + " mA @ ";
            label += context.parsed.x.toFixed(2) + " mW";
          }
          return label;
        },
      },
    },
  };

  // Options block:
  const options = {
    aspectRatio: 1.5,
    animation: {
      duration: 0,
    },
    hover: {
      mode: "nearest",
      intersect: true,
    },
    scales: {
      y: {
        title: {
          display: true,
          text: "RSSI current [mA]",
        },
        min: 0,
        max: 0.25
      },
      x: {
        title: {
          display: true,
          text: xTitle,
        },
        min: 0,
        max: 0.5
      },
    },
    plugins,
  };

  const chart = new Chart(RSSIplot, {
    type: "scatter",
    data,
    options,
  });
}
