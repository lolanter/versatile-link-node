function plotStatus(statusData, chartCanvas) {
  const plot = document.getElementById(chartCanvas);

  const borderWidth = 0;

  // Data block:
  let datasets = [];
  datasets.push({
    label: "SAP",
    borderColor: 'rgba(138, 138, 138, 0.5)',
    backgroundColor: 'rgba(138, 138, 138, 0.5)',
    data: statusData.sap,
    pointRadius: 0,
    showLine: true,
    borderWidth: borderWidth,
    stepped: true,
    fill: true,
  });
  datasets.push({
    label: "PRE",
    borderColor: "lightblue",
    backgroundColor: "lightblue",
    data: statusData.pre,
    pointRadius: 0,
    showLine: true,
    borderWidth: borderWidth,
    stepped: true,
    fill: true,
  });
  datasets.push({
    label: "POST",
    borderColor: "darksalmon",
    backgroundColor: "darksalmon",
    data: statusData.post,
    pointRadius: 0,
    showLine: true,
    borderWidth: borderWidth,
    stepped: true,
    fill: true,
  });

  const data = {
    datasets,
  };

  // Plugins:
  const plugins = {
    tooltip: {
      enabled: true,
      callbacks: {
        title: function(context) {
          console.log(context)
          let title = [context[0].label];
          title.push('Lot size: ' + statusData.all[context[0].dataIndex])
          return title;
        },
        label: function (context) {
          let label = context.dataset.label || '';
          if (label) {
            label += ': ';
          }
          if (context.parsed.y !== null) {
            label += Math.round(context.parsed.y * statusData.all[context.dataIndex] / 100);
          }
          return label;
        }
      }
    },
  };

  // Options block:
  const options = {
    plugins,
    responsive: true,
    scales: {
      x: {
        stacked: true,
      },
      y: {
        stacked: false,
        title: {
          display: true,
          text: '%'
        }
      }
    },
    animation: {
      duration: 0
    },
    interaction: {
      mode: 'x'
    }
  };

  const chart = new Chart(plot, {
    type: "bar",
    data: data,
    options,
  });

  return chart;
}

function plotBurnin(burninData, chartCanvas, yMin, yMax) {

  const burninPlot = document.getElementById(chartCanvas);

  const data = {
    labels: burninData.labels,
    datasets: [
      {
        label: 'Ch 1',
        data: burninData.dataArrayCh1,
        backgroundColor: 'blue'
      },
      {
        label: 'Ch 2',
        data: burninData.dataArrayCh2,
        backgroundColor: 'orange'
      },
      {
        label: 'Ch 3',
        data: burninData.dataArrayCh3,
        backgroundColor: 'green'
      },
      {
        label: 'Ch 4',
        data: burninData.dataArrayCh4,
        backgroundColor: 'saddlebrown'
      },
    ],
  };

  const chart = new Chart(burninPlot, {
    type: 'boxplot',
    data,
    options: {
      scales: {
        y: {
          min: yMin,
          max: yMax,
        }
      }
    }
  });
}

function plotBurninPrePost(burninData, failsData, chartCanvas, yMin, yMax) {

  const burninPlot = document.getElementById(chartCanvas);

  const data = {
    labels: burninData.labels,
    datasets: [
      {
        label: 'Ch 1',
        data: burninData.dataArrayCh1,
        backgroundColor: 'blue'
      },
      {
        label: 'Ch 2',
        data: burninData.dataArrayCh2,
        backgroundColor: 'orange'
      },
      {
        label: 'Ch 3',
        data: burninData.dataArrayCh3,
        backgroundColor: 'green'
      },
      {
        label: 'Ch 4',
        data: burninData.dataArrayCh4,
        backgroundColor: 'saddlebrown'
      },
      {
        label: 'Min Fails',
        type: 'bar',
        yAxisID: 'yFailMin',
        data: failsData.sumMinFails,
        stack: true,
        backgroundColor: 'red'
      },
      {
        label: 'Max Fails',
        type: 'bar',
        yAxisID: 'yFailMax',
        data: failsData.sumMaxFails,
        stack: true,
        backgroundColor: 'deeppink'
      },
      {
        label: 'No Pre Data',
        type: 'bar',
        yAxisID: 'yFailMin',
        data: failsData.panelPreNulls,
        stack: true,
        backgroundColor: 'LightGrey'
      },

      {
        label: 'No Post Data',
        type: 'bar',
        yAxisID: 'yFailMax',
        data: failsData.panelPostNulls,
        stack: true,
        backgroundColor: 'DimGray'
      },
    ],
  };

  const chart = new Chart(burninPlot, {
    type: 'boxplot',
    data,
    options: {
      scales: {
        yAxes: [
          {
            id: 'yFailMin',
            type: 'linear'
          },
          {
            id: 'yFailMax',
            type: 'linear',
          }],
        y: {
          min: yMin,
          max: yMax,
        },
        yFailMin: {
          display: false,
          min: 0,
          max: 40,
          grid: {
            display: false
          }
        },
        yFailMax: {
          display: false,
          min: 0,
          max: 40,
          grid: {
            display: false
          },
          reverse: true
        }
      }
    }
  });
}