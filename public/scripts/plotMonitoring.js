function plotMonitoring(paramdata, chartCanvas, sampleId, yMin, yMax, timeSpan) {
  const plot = document.getElementById(chartCanvas);

  const colors = ["red", "blue", "green", "orange"];
  const borderWidth = 1;

  let unit = "month";
  if(timeSpan < 31){
    unit = "week";
  } 
  if(timeSpan < 14) {
    unit = "day"
  }

  // Data block:
  let datasets = [];
  for (let ch = 0; ch < paramdata.length; ch++) {
    datasets.push({
      label: "param",
      borderColor: colors[ch],
      backgroundColor: colors[ch],
      data: paramdata[ch],
      pointRadius: 1,
      showLine: false,
      borderWidth: borderWidth,
    });
  }

  const data = {
    datasets,
  };

  // Plugins:
  const plugins = {
    legend: {
      display: false,
    },
    tooltip: { enabled: false },
    hover: { mode: null },
  };

  // Options block:
  const options = {
    _sampleId: sampleId,
    plugins,
    scales: {
      x: {
        type: "linear",
        time: {
          unit: unit,
          displayFormats: {
            quarter: "MMM YYYY",
          },
        },
      },
      y: {
        min: yMin,
        max: yMax,
      },
    },
    animation: {
      duration: 0,
    },
  };

  const chart = new Chart(plot, {
    type: "scatter",
    data: data,
    options,
  });

  return chart;
}

// UTILITY FUNCTIONS

function updateMovingAvg(event) {
  const sampleId = event.target.id.replace("Avg", "Id");
  const dataId = event.target.id.replace("Avg", "Data");
  const valueId = event.target.id + "Value";

  let chart = Object.values(Chart.instances).find(
    (c) => c.options._sampleId === sampleId
  );

  const avgValue = parseInt(document.getElementById(event.target.id).value);

  document.getElementById(valueId).textContent = avgValue;

  const datasets = JSON.parse(document.getElementById(dataId).innerHTML);

  for (let ch = 0; ch < datasets.datasetDate.length; ch++) {
    let dataset = datasets.datasetDate[ch];
    if (chart.options.scales.x.type === "linear") {
      dataset = datasets.datasetId[ch];
    }

    let newDataset = [];
    let sumArray = [];
    for (let i = 0; i < dataset.length; i++) {
      sumArray.push(dataset[i].y);
      if (i >= avgValue) {
        newDataset.push({
          x: dataset[i].x,
          y:
            sumArray.reduce((partialSum, a) => partialSum + a, 0) /
            (avgValue + 1),
        });
        sumArray.shift();
      }
    }
    chart.data.datasets[ch].data = newDataset;
  }
  chart.update();
}

function toggleData(id, sampleId) {
  const chart = Object.values(Chart.instances).find(
    (c) => c.options._sampleId === sampleId
  );

  document.getElementById(sampleId.replace("Id", "Avg")).value = 0;
  document.getElementById(
    sampleId.replace("Id", "Avg") + "Value"
  ).textContent = 0;

  const dataId = sampleId.replace("Id", "Data");
  const datasets = JSON.parse(document.getElementById(dataId).innerHTML);

  for (let ch = 0; ch < datasets.datasetId.length; ch++) {
    if (id) {
      chart.data.datasets[ch].data = datasets.datasetId[ch];
      chart.options.scales.x.type = "linear";
      chart.options.scales.x.ticks = {}
    } else {
      chart.data.datasets[ch].data = datasets.datasetDate[ch];
      chart.options.scales.x.type = "time";
      chart.options.scales.x.ticks = {
        callback: function(val) {
          var t = new Date(1970, 0, 1); // Epoch
          t.setSeconds(val/1000);
          const day = t.getDate();
          const month = t.getMonth()+1;
          const year = t.getFullYear();
         
          return day+"/"+month+"/"+year;
        }
      }
    }
  }
  chart.update();
}